﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class MainMenuCam : MonoBehaviour
{
    [SerializeField]
    private float _CamSpeed;

    private CinemachineVirtualCamera _MenuCam;
    private float _FinalPos = 3.0f;
    private float _CurrentPos;
    private CinemachineTrackedDolly _CinemachineTrackedDolly;
    private bool _HasReached;

    private void Awake()
    {
        _MenuCam = GetComponent<CinemachineVirtualCamera>();
        _CinemachineTrackedDolly = _MenuCam.GetCinemachineComponent<CinemachineTrackedDolly>();
    }

    private void Update()
    {
         MoveCam();
    }

    private void MoveCam()
    {
        if (_CinemachineTrackedDolly.m_PathPosition < _FinalPos && _HasReached == false)
        {
            _CurrentPos += Time.deltaTime * _CamSpeed;
        }
        else 
        {
            if (_HasReached == true)
            {
                _CurrentPos -= Time.deltaTime * _CamSpeed;
            }
        }

        if (_CinemachineTrackedDolly.m_PathPosition >= _FinalPos)
        {
            _HasReached = true;
        }

        if (_CinemachineTrackedDolly.m_PathPosition <= 0.0f)
        {
            _HasReached = false;
        }

        _CinemachineTrackedDolly.m_PathPosition = _CurrentPos;
    }
}
