﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOrientationCheck : MonoBehaviour
{
    [SerializeField]
    private RefBool _IsMeshVisibleBool;
    private MeshRenderer _OrientationCheck;
    private void Awake()
    {
        _OrientationCheck = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        IsMeshVisible();
    }

    private void IsMeshVisible()
    {
        _IsMeshVisibleBool.Value = _OrientationCheck.isVisible;
    }
}
