﻿using Cinemachine;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField] 
    private RefVector2 _CameraInputVector;
    [SerializeField]
    private RefBool _IsMeshVisibleBool;
    [SerializeField]
    private RefBool _IsWallRiding;

    [SerializeField]
    private RefRespawnManager _RespawnManager;
    [SerializeField]
    private RefCameraManger _CurrentCameraManager;
    
    [SerializeField, Range(0.0f, 10.0f)]
    private float _XRecenterTime; 
    [SerializeField, Range(0.0f, 10.0f)]
    private float _YRecenterTime; 
    [SerializeField, Range(0.0f, 10.0f)]
    private float _XWaitTime; 
    [SerializeField, Range(0.0f, 10.0f)]
    private float _YWaitTime;
    [SerializeField, Range(0.0f,10.0f)]
    private float _DefaultXRecenterTime;
    [SerializeField, Range(0.0f, 10.0f)]
    private float _DefaultYRecenterTime;
    [SerializeField, Range(0.0f, 10.0f)]
    private float _DefaultXWaitTime; 
    [SerializeField, Range(0.0f, 10.0f)]
    private float _DefaultYWaitTime;    
    [SerializeField, Range(1.0f, 10.0f)]
    private float _DefaultYSensitivity;
    [SerializeField, Range(1.0f, 700.0f)]
    private float _DefaultXSensitivity;
    
    [SerializeField]
    private RefBool _IsVerticalInvert;
    [SerializeField]
    private RefFloat _CameraSensitivityX;
    [SerializeField]
    private RefFloat _CameraSensitivityY;

    [SerializeField]
    private RefUserInterfaceManager _RefUIManager; 
    private CinemachineComponentBase _CMBase;

    private CinemachineFreeLook _CM;
    
    private void Awake()
    {
        CinemachineCore.GetInputAxis = GetInputAxis;
        _CM = GetComponent<CinemachineFreeLook>();
        if (_CurrentCameraManager.Value == null)
        {
            _CurrentCameraManager.Value = this;
        }
    }

    private void OnEnable()
    {
        if (_RefUIManager.Value == null)
        {
            return;
        }
        _RespawnManager.Value.OnDeath += StopFollowingPlayer;
    }

    private void OnDisable()
    {
        if (_RefUIManager.Value == null)
        {
            return;
        }
        _RespawnManager.Value.OnDeath -= StopFollowingPlayer;
    }

    private void Start()
    {
        
        SetDefaultRecenterAndWaitTime();

        if (_RefUIManager.Value == null)
        {
            _IsVerticalInvert.Value = true;
            _CM.m_YAxis.m_MaxSpeed = _DefaultYSensitivity;
            _CM.m_XAxis.m_MaxSpeed = _DefaultXSensitivity;
        }
       
    }
    
    private void Update()
    {
        ZTranslationDampingController();
        RotateOnWallRide();
        SetVerticalInvert();
        SetCameraSensitivityY();
        SetSetCameraSensitivityX();
    }

    private void StopFollowingPlayer()
    {
        _CM.m_Follow = null;
    }

    public void TransitionToQTECamera()
    {
        _CM.m_Priority = 6;
    }

    public void TransitionToPlayerCamera()
    {
        _CM.m_Priority = 10;
    }

    private void ZTranslationDampingController()
    {
        // if (_IsMeshVisibleBool.Value == true)
        // {
        //     _CM.GetRig(0).GetCinemachineComponent<CinemachineOrbitalTransposer>().m_ZDamping = 0.0f;
        // }
        // else
        // {
        //     _CM.GetRig(0).GetCinemachineComponent<CinemachineOrbitalTransposer>().m_ZDamping = 1.0f;
        // }
    }

    private float GetInputAxis(string axisName)
    {
         if (axisName == "Look X")
         {
             return _CameraInputVector.Value.x;
         }
         
         if (axisName == "Look Y")
         {
             return _CameraInputVector.Value.y;
         }
         
         return 0.0f;
    }

    private void RotateOnWallRide()
    {
        if (_CameraInputVector.Value.magnitude == 0.0f)
        {
            if (_IsWallRiding.Value == true)
            {
                WallRideRecenter();
              
            }
            else
            {
                SetDefaultRecenterAndWaitTime();
            }
        }
    }

    private void SetVerticalInvert()
    {
        if (_RefUIManager.Value != null)
        {
            _CM.m_YAxis.m_InvertInput = _IsVerticalInvert.Value;
        }
    }

    private void SetCameraSensitivityY()
    {
        if (_RefUIManager.Value != null)
        { 
            _CM.m_YAxis.m_MaxSpeed = _CameraSensitivityY.Value;
        }
    }

    private void SetSetCameraSensitivityX()
    {
        if (_RefUIManager.Value != null)
        { 
            _CM.m_XAxis.m_MaxSpeed = _CameraSensitivityX.Value;
        }
    }

    private void SetDefaultRecenterAndWaitTime()
    {
        _CM.m_RecenterToTargetHeading.m_RecenteringTime = _DefaultXRecenterTime;
        _CM.m_RecenterToTargetHeading.m_WaitTime = _DefaultXWaitTime;
        _CM.m_YAxisRecentering.m_RecenteringTime = _DefaultYRecenterTime;
        _CM.m_YAxisRecentering.m_WaitTime = _DefaultYWaitTime;
    }


    private void WallRideRecenter()
    {
        _CM.m_RecenterToTargetHeading.m_RecenteringTime = _XRecenterTime;
        _CM.m_RecenterToTargetHeading.m_WaitTime = _XWaitTime;
        _CM.m_YAxisRecentering.m_RecenteringTime = _YRecenterTime;
        _CM.m_YAxisRecentering.m_WaitTime = _YWaitTime;
    }
}
