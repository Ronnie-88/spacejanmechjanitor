﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garbage : MonoBehaviour
{
    /*
     * responsible for giving a score to the player when they collide with the object
     */
    [SerializeField]
    private RefFloat _NewScore;
    
    [SerializeField,Range(0, 2000)]
    private float _ScoreToAdd;

    [SerializeField]
    private GameObject _BubbleParticles;

    
    
    

    private void OnTriggerEnter(Collider other)
    {
        PlayerControllerManager player = other.gameObject.GetComponent<PlayerControllerManager>();

        if (player != null)
        { 
            AddToCurrentScore();
            player.InvokeAddToScore(); 
            Destroy(gameObject);
        }
    }


    private void AddToCurrentScore()
    {
        _NewScore.Value += _ScoreToAdd;
        Instantiate(_BubbleParticles, transform.position, Quaternion.identity);
    }
}
