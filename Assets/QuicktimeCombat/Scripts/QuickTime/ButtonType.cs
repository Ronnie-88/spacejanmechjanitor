using UnityEngine;

[System.Serializable]
public class ButtonType
{
    public string ButtonName;
    public InputType MappedInputType;
    public Sprite Sprite;
}