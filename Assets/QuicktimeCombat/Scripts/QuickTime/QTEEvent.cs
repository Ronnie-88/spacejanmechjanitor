﻿using System.Collections;
using ObjectPooling;
using UnityEngine;

public class QTEEvent : MonoBehaviour
{
    [Header("SO References")]
    [SerializeField]
    private RefQTEBehaviour _QTEBehaviourAsset;
    [SerializeField]
    private RefBool _IsQTEActive;

    [Header("Prefabs")]
    [SerializeField]
    private QTEButton _QTEButton;

    [Header("Slowmo Variables")]
    [SerializeField]
    private AnimationCurve _SlowDown;
    [SerializeField]
    private AnimationCurve _SpeedUp;
    [SerializeField, Range(0.1f, 2f)]
    private float _SlowDownTime;
    [SerializeField, Range(0.1f, 2f)]
    private float _SpeedUpTime;

    private CanvasGroup _CanvasGroup;
    private IEnumerator _CurrentCoroutine;

    public Event OnSpeedUpEnd;

    #region Initializing Methods
    private void Awake()
    {
        GetComponents();
        CreatePoolManager();
    }

    private void GetComponents()
    {
        _QTEBehaviourAsset.Value = GetComponentInChildren<QTEBehaviour>(true);
        _CanvasGroup = GetComponent<CanvasGroup>();
    }

    private void CreatePoolManager()
    {
        PoolManager.CreatePool(_QTEButton, 6);
    }
    #endregion

    #region Coroutine Methods
    private IEnumerator TimeWarp(AnimationCurve curve, float timeModifier, bool activatingQTE = false)
    {
        float curveValue = 0f;

        // Used to make the buttons invisible.
        if (activatingQTE == true)
        {
            _CanvasGroup.alpha = 0f;
        }

        while (curveValue < 1f)
        {
            Time.timeScale = curve.Evaluate(curveValue);
            Time.fixedDeltaTime = Time.timeScale * 0.02f;

            // Multiply the time modifier by fractions to make it slower or inttegers to make it faster.
            curveValue += Time.fixedUnscaledDeltaTime * timeModifier;

            // Updates the buttons visibility.
            if (activatingQTE == true)
            {
                _CanvasGroup.alpha = curveValue;
            }

            yield return null;
        }

        // Restores the buttons visibility to 1f.  
        if (activatingQTE == true)
        {
            _CanvasGroup.alpha = 1f;
            _IsQTEActive.Value = true;
        }

        Time.timeScale = curve.Evaluate(1f);
        Time.fixedDeltaTime = Time.timeScale * 0.02f;

        StopInternalCoroutine();
    }

    public void SpeedUp()
    {
        StartInternalCoroutine(TimeWarp(_SpeedUp, _SpeedUpTime));
    }

    public void SlowDown()
    {
        StartInternalCoroutine(TimeWarp(_SlowDown, _SlowDownTime, true));
    }

    // Evaluates a single internal coroutine.
    private void StartInternalCoroutine(IEnumerator coroutine)
    {
        if (_CurrentCoroutine != null)
        {
            StopCoroutine(_CurrentCoroutine);
        }

        _CurrentCoroutine = coroutine;
        StartCoroutine(_CurrentCoroutine);
    }

    // Stops the internal coroutine. Tooltip: Use at the end of interchangeable single-use coroutines.
    private void StopInternalCoroutine()
    {
        StopCoroutine(_CurrentCoroutine);
        _CurrentCoroutine = null;
    }
    #endregion
}
