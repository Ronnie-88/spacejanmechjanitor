﻿using UnityEngine;

public class EngageCombatTrigger : MonoBehaviour
{
    [SerializeField]
    private AIBot _Bot;

    [SerializeField]
    private RefTransform _CombatPosition;

    [SerializeField]
    private RefBool _IsInCombatBool;

    [SerializeField]
    private RefQTEBehaviour _QTEBehaviourAsset;

    #region Unity Methods
    private void OnDisable()
    {
        //_IsInCombatBool.Value = false;
    }

    private void Awake()
    {
        _Bot = GetComponentInParent<AIBot>();
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerControllerManager player = other.gameObject.GetComponent<PlayerControllerManager>();

        if (player != null)
        {
            _IsInCombatBool.Value = true;
            _QTEBehaviourAsset.Value.gameObject.SetActive(true);
            _QTEBehaviourAsset.Value.OnSuccess += _Bot.StartEnemyDeathSequence;
            _QTEBehaviourAsset.Value.OnFail += _Bot.UpdateWaypointTarget;
            _QTEBehaviourAsset.Value.OnFail += _Bot.EnableMovement;

            SetCombatPosition();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerControllerManager player = other.gameObject.GetComponent<PlayerControllerManager>();

        if (player != null)
        {
           // _QTEBehaviourAsset.Value.OnSuccess -= _Bot.StartEnemyDeathSequence;
            _QTEBehaviourAsset.Value.OnFail -= _Bot.UpdateWaypointTarget;
            _QTEBehaviourAsset.Value.OnFail -= _Bot.EnableMovement;
        }
    }
    #endregion

    #region Engage Combat Methods
    private void SetCombatPosition()
    {
        _CombatPosition.Value = transform;
    }
    #endregion
}
