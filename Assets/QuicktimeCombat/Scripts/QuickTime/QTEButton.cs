﻿using ObjectPooling;
using UnityEngine;
using UnityEngine.UI;

public class QTEButton : PoolableObject
{
    private Button _Button;

    private void Awake()
    {
        GetComponents();
    }

    private void OnEnable()
    {
        _Button.interactable = true;
    }

    private void GetComponents()
    {
        _Button = GetComponent<Button>();
    }

    public void SetSprite(Sprite sprite)
    {
        _Button.image.sprite = sprite;
    }

    public void Succeeded()
    {
        _Button.interactable = false;
    }
}
