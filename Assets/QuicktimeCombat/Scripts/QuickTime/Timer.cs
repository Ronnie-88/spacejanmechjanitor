using System;

public class Timer
{
    public event Action OnTimerEnd;
    public float Time { get; private set; }
    private bool _isActive = false;

    public void StartTimer(float duration)
    {
        Time = duration;
        _isActive = true;
    }

    public void StopTimer()
    {
        _isActive = false;
    }

    public void Tick(float deltaTime)
    {
        if (Time <= 0f || _isActive == false) return;

        Time -= deltaTime;
        if (Time <= 0f)
        {
            Time = 0f;
            OnTimerEnd?.Invoke();
        }
    }

}