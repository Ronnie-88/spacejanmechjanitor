using UnityEngine;

public class InputTypeGenerator
{
    private InputType[] _Inputs = new InputType[4];

    public InputTypeGenerator()
    {
        _Inputs[0] = InputType.North;
        _Inputs[1] = InputType.East;
        _Inputs[2] = InputType.West;
        _Inputs[3] = InputType.South;
    }
    
    public InputType GetRandom()
    {
        int i = Random.Range(0, _Inputs.Length);
        return _Inputs[i];
    }

}
