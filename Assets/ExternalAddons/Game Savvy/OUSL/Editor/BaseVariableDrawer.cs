﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System;

namespace GameSavvy.ScriptableLibrary
{
	[CustomPropertyDrawer(typeof(BoolVariable))]
	[CustomPropertyDrawer(typeof(ByteVariable))]
	[CustomPropertyDrawer(typeof(UShortVariable))]
	[CustomPropertyDrawer(typeof(UIntVariable))]
	[CustomPropertyDrawer(typeof(ULongVariable))]
	[CustomPropertyDrawer(typeof(SByteVariable))]
	[CustomPropertyDrawer(typeof(ShortVariable))]
	[CustomPropertyDrawer(typeof(IntVariable))]
	[CustomPropertyDrawer(typeof(LongVariable))]
	[CustomPropertyDrawer(typeof(FloatVariable))]
	[CustomPropertyDrawer(typeof(DoubleVariable))]
	[CustomPropertyDrawer(typeof(Vector2Variable))]
	[CustomPropertyDrawer(typeof(Vector3Variable))]
	[CustomPropertyDrawer(typeof(StringVariable))]
	[CustomPropertyDrawer(typeof(TransformVariable))]
	public class BaseVariableDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect rect, SerializedProperty prop, GUIContent lbl)
		{
			SerializedProperty constRefIndex = prop.FindPropertyRelative("UseConstant");
			SerializedProperty constVal = prop.FindPropertyRelative("ConstantValue");
			SerializedProperty refVar = prop.FindPropertyRelative("ReferenceVar");

			Color colorToSet = EditorGUIUtility.isProSkin ?
				Color.Lerp(Color.gray, Color.black, 0.61f):
				Color.Lerp(Color.gray, Color.white, 0.45f);
			EditorGUI.DrawRect(rect,  colorToSet);
			rect.x += 24;
			rect.width -= 24;
			EditorGUIUtility.labelWidth -= 24;

			EditorGUI.BeginProperty(rect, lbl, prop);
			int indentLvl = EditorGUI.indentLevel;
			{
				EditorGUI.indentLevel = 0;
				int index = constRefIndex.boolValue ? 0 : 1;
				EditorGUI.PropertyField(rect, index == 0 ? constVal : refVar, lbl);
				string[] tagArr = new string[2] { "Constant", "RefVariable" };
				index = EditorGUI.Popup(new Rect(rect.x-24, rect.y, 16, rect.height), "", index, tagArr);
				constRefIndex.boolValue = index == 0;
			}
			EditorGUI.indentLevel = indentLvl;
			EditorGUI.EndProperty();
		}
	}


	[CustomPropertyDrawer(typeof(QuaternionVariable))]
	public class QuaternionVariableDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect rect, SerializedProperty prop, GUIContent lbl)
		{
			SerializedProperty constRefIndex = prop.FindPropertyRelative("UseConstant");
			SerializedProperty constVal = prop.FindPropertyRelative("ConstantValue");
			SerializedProperty refVar = prop.FindPropertyRelative("ReferenceVar");

			Color colorToSet = EditorGUIUtility.isProSkin ?
				Color.Lerp(Color.gray, Color.black, 0.61f):
				Color.Lerp(Color.gray, Color.white, 0.45f);
			EditorGUI.DrawRect(rect,  colorToSet);
			rect.x += 24;
			rect.width -= 24;
			EditorGUIUtility.labelWidth -= 24;

			EditorGUI.BeginProperty(rect, lbl, prop);
			int indentLvl = EditorGUI.indentLevel;
			{
				EditorGUI.indentLevel = 0;
				int index = constRefIndex.boolValue ? 0 : 1;
				if (index == 0)
				{
					Vector3 v3 = constVal.quaternionValue.eulerAngles;
					v3 = new Vector3((float)Math.Round(v3.x, 2), (float)Math.Round(v3.y, 2), (float)Math.Round(v3.z, 2));
					v3 = EditorGUI.Vector3Field(rect, lbl, v3);
					constVal.quaternionValue = Quaternion.Euler(v3);
				}
				else
				{
					EditorGUI.PropertyField(rect, refVar, lbl);
				}
				string[] tagArr = new string[2] { "Constant", "RefVariable" };
				index = EditorGUI.Popup(new Rect(rect.x - 24, rect.y, 16, rect.height), "", index, tagArr);
				constRefIndex.boolValue = index == 0;
			}
			EditorGUI.indentLevel = indentLvl;
			EditorGUI.EndProperty();
		}
	}

}

#endif
