﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameSavvy.ScriptableLibrary.RuntimeSets
{

    [System.Serializable]
    public abstract class RuntimeSet<T> : ScriptableObject
    {

#pragma warning disable 414
        [Tooltip("HashSet<T> : Unique items cannot be repeated")]
        // _TODO: [ReorderableList]
        public HashSet<T> Items = new HashSet<T>();
#pragma warning restore 414

        public event Action<T> OnItemAdded;
        public event Action<T> OnItemRemoved;

        public virtual void AddItem(T item)
        {
            if (item != null && Items.Contains(item) == false)
            {
                Items.Add(item);
                if (OnItemAdded != null)
                {
                    OnItemAdded.Invoke(item);
                }
            }
        }

        public virtual void RemoveItem(T item)
        {
            if (item != null && Items.Contains(item))
            {
                Items.Remove(item);
                if (OnItemRemoved != null)
                {
                    OnItemRemoved.Invoke(item);
                }
            }
        }

        public virtual bool ContainsItem(T item)
        {
            return item != null && Items.Contains(item);
        }

        public virtual void CleanNulls()
        {
            Items.RemoveWhere((t) => t == null);
        }

    }

}
