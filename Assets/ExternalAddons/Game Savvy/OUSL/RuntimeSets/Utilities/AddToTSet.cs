﻿using UnityEngine;

namespace GameSavvy.ScriptableLibrary.RuntimeSets
{

	public abstract class AddToTSet<T, RTS> : MonoBehaviour where RTS : RuntimeSet<T>
	{

		[SerializeField, Tooltip("False = Awake/OnDestroy;\nTrue = OnEnable/OnDisable")]
		protected bool _OnlyIfEnabed = false;

		[SerializeField]
		protected RTS _Set;

		protected T _ToAdd;

		protected virtual void Awake()
		{
			_ToAdd = GetComponent<T>();
			if(_OnlyIfEnabed == false && _ToAdd != null)
			{
				_Set.AddItem(_ToAdd);
			}
		}

		protected void OnEnable()
		{
			if(_OnlyIfEnabed && _ToAdd != null)
			{
				_Set.AddItem(_ToAdd);
			}
		}

		protected void OnDisable()
		{
			if(_OnlyIfEnabed && _ToAdd != null)
			{
				_Set.RemoveItem(_ToAdd);
			}
		}

		protected void OnDestroy()
		{
			if(_OnlyIfEnabed == false && _ToAdd != null)
			{
				_Set.RemoveItem(_ToAdd);
			}
		}

	}
}
