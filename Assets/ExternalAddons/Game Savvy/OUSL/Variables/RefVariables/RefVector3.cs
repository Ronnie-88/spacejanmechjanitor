using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Vector3")]
	public class RefVector3 : RefVariable<Vector3> { }
}
