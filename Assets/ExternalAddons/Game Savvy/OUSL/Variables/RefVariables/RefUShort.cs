using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/UShort")]
	public class RefUShort : RefVariable<ushort> { }
}
