using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/GameObject")]
	public class RefGameObject : RefVariable<GameObject> {}
}
