using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Byte")]
	public class RefByte : RefVariable<byte> { }
}
