using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Quaternion")]
	public class RefQuaternion : RefVariable<Quaternion> { }
}
