using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/SByte")]
	public class RefSByte : RefVariable<sbyte> { }
}
