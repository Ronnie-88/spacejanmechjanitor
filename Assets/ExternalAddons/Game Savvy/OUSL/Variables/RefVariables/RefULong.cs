using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/ULong")]
	public class RefULong : RefVariable<ulong> { }
}
