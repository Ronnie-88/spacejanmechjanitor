using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Rigidbody")]
    public class RefRigidbody : RefVariable<Rigidbody> {}
}
