﻿using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefFloatChanged : OnRefVariableChanged<float, RefFloat, UEvent_Float> { }
}
