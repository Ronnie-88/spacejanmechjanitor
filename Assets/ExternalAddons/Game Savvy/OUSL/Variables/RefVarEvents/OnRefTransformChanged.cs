﻿using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefTransformChanged : OnRefVariableChanged<Transform, RefTransform, UEvent_Transform> { }
}
