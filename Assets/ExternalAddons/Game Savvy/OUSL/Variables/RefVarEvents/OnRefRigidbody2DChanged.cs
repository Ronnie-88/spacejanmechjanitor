﻿using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefRigidbody2DChanged : OnRefVariableChanged<Rigidbody2D, RefRigidbody2D, UEvent_Rigidbody2D> { }
}
