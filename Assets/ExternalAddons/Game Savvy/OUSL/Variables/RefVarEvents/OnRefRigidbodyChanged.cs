﻿using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefRigidbodyChanged : OnRefVariableChanged<Rigidbody, RefRigidbody, UEvent_Rigidbody> { }
}
