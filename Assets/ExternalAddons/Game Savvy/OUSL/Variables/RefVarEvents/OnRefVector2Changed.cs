using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefVector2Changed : OnRefVariableChanged<Vector2, RefVector2, UEvent_Vector2> { }
}
