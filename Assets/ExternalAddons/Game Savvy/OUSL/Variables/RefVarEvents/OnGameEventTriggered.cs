﻿using GameSavvy.OpenUnityAttributes;
using GameSavvy.ScriptableLibrary;
using UnityEngine;
using UnityEngine.Events;


public class OnGameEventTriggered : MonoBehaviour
{
	[SerializeField]
    [Required]
    [OnlyAssets]
	private GameEvent _GameEvent;

	[SerializeField]
	private UnityEvent _Actions;

	private void OnEnable()
	{
		_GameEvent.Observers += TriggerActions;
	}

	private void OnDisable()
	{
		_GameEvent.Observers -= TriggerActions;
	}

	[Button]
	public void TriggerActions()
	{
		_Actions.Invoke();
	}
	
}
