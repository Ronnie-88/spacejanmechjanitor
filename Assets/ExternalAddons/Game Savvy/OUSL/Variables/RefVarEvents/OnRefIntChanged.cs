using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefIntChanged : OnRefVariableChanged<int, RefInt, UEvent_Int> { }
}
