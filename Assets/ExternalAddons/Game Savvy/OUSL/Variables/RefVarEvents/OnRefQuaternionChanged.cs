using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefQuaternionChanged : OnRefVariableChanged<Quaternion, RefQuaternion, UEvent_Quaternion> { }
}
