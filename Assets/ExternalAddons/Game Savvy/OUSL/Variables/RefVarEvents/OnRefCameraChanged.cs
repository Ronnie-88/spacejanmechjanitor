﻿using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefCameraChanged : OnRefVariableChanged<Camera, RefCamera, UEvent_Camera> { }
}
