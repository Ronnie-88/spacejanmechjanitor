﻿using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefDoubleChanged : OnRefVariableChanged<double, RefDouble, UEvent_Double> { }
}
