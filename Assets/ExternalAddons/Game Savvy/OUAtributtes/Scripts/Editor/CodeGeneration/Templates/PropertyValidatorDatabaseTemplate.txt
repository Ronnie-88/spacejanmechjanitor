﻿// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class __classname__
    {
        private static Dictionary<Type, PropertyValidator> _ValidatorsByAttributeType;

        static __classname__()
        {
            _ValidatorsByAttributeType = new Dictionary<Type, PropertyValidator>();
            __entries__
        }

        public static PropertyValidator GetValidatorForAttribute(Type attributeType)
        {
            PropertyValidator validator;
            if (_ValidatorsByAttributeType.TryGetValue(attributeType, out validator))
            {
                return validator;
            }
            else
            {
                return null;
            }
        }
    }
}
