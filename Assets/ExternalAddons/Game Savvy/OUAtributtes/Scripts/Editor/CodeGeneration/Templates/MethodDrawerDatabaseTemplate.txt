﻿// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class __classname__
    {
        private static Dictionary<Type, MethodDrawer> _DrawersByAttributeType;

        static __classname__()
        {
            _DrawersByAttributeType = new Dictionary<Type, MethodDrawer>();
            __entries__
        }

        public static MethodDrawer GetDrawerForAttribute(Type attributeType)
        {
            MethodDrawer drawer;
            if (_DrawersByAttributeType.TryGetValue(attributeType, out drawer))
            {
                return drawer;
            }
            else
            {
                return null;
            }
        }
    }
}
