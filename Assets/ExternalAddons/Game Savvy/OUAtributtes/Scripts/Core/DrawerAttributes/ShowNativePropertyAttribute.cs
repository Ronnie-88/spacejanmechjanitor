﻿using System;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ShowNativePropertyAttribute : DrawerAttribute
    {

    }
}
