﻿using UnityEngine;

public class UIPauseMenuPlayer : UIGenericPlayer
{
    // [Header("Pause Menu")]
    // [SerializeField]
    // private AK.Wwise.Event _Resume = null;
    // [SerializeField]
    // private AK.Wwise.Event _MainMenu = null;
    // [SerializeField]
    // private AK.Wwise.Event _Quit = null;
    // [SerializeField]
    // private AK.Wwise.Event _Pause;
    // [SerializeField]
    // private AK.Wwise.Event _Unpause;

    public void PlayResume() {}
    //  => _Resume.Post(this.gameObject);
    public void PlayMainMenu() {}
    // => _MainMenu.Post(this.gameObject);
    public void PlayQuit() {}
    // => _Quit.Post(this.gameObject);

    private void OnEnable() {}
    // => _Pause.Post(this.gameObject);
    public void OnUnpause() {}
    // => _Unpause.Post(this.gameObject);
}
