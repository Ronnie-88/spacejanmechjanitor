﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour
{
    // [SerializeField]
    // private StateManager _StateManager = null;

    // [SerializeField]
    // private AK.Wwise.Event _Music = null;

    private void Start()
    {
        // // Get reference to StateManager
        // if (_StateManager == null)
        // {
        //     _StateManager = GameObject.Find("Wwise").GetComponent<StateManager>();
        // }

        // // Set default music track to the Menu music
        // _StateManager._GeneralGameStates[0].SetValue(); // GeneralGameState.MainMenu

        // // Start music if reference is set
        // if (_Music != null)
        // {
        //     _Music.Post(this.gameObject);
        // }

        // Subscription
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // For Debug purporses
    private void Update()
    {
        // if (Input.GetKeyDown(KeyCode.Space))
        // {
        //     _Music.Post(this.gameObject);
        // }
        // if (Input.GetKeyDown(KeyCode.Alpha1))
        // {
        //     _StateManager._GeneralGameStates[0].SetValue(); // GeneralGameState.MainMenu
        // }
        // if (Input.GetKeyDown(KeyCode.Alpha2))
        // {
        //     _StateManager._GeneralGameStates[1].SetValue(); // GeneralGameState.Gameplay
        // }
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // switch (scene.buildIndex)
        // {
        //     case 0:
        //         _StateManager._GeneralGameStates[0].SetValue(); // GeneralGameState.MainMenu
        //         break;
        //     case 1:
        //         _StateManager._GeneralGameStates[0].SetValue(); // GeneralGameState.MainMenu
        //         break;
        //     case 2:
        //         _StateManager._GeneralGameStates[0].SetValue(); // GeneralGameState.MainMenu
        //         break;
        //     case 3:
        //         _StateManager._GeneralGameStates[1].SetValue(); // GeneralGameState.Gameplay
        //         _StateManager._CurrentLevelStates[0].SetValue(); // CurrentLevelStates.Level_01
        //         break;
        //     case 4:
        //         _StateManager._GeneralGameStates[1].SetValue(); // GeneralGameState.Gameplay
        //         _StateManager._CurrentLevelStates[0].SetValue(); // CurrentLevelStates.Level_01
        //         break;
        //     default:
        //         Debug.Log($"{this.gameObject.name} | Invalid State change detected. Scene index is invalid.");
        //         break;
        // }
    }
}
