﻿using System.Collections.Generic;
using UnityEngine;

namespace ObjectPooling
{
    public static class PoolManager
    {
        // Initial size of the pool.
        private static readonly int _INITIAL_POOL_SIZE = 10;

        // Static dictionary that will contain all the poolable objects.
        private static Dictionary<int, Queue<PoolableObject>> _PoolQueue = new Dictionary<int, Queue<PoolableObject>>();

        // Add object to the poolable queue.
        private static PoolableObject AddObjectToPool(PoolableObject prefab, int poolId)
        {
            var clone = GameObject.Instantiate(prefab);
            clone.gameObject.SetActive(false);
            clone.PoolIdIdentifier = poolId;
            _PoolQueue[poolId].Enqueue(clone);
            return clone;
        }

        // Add PoolableObject to queue again. 
        public static void AddObjectToQueue(PoolableObject poolObject, int poolId)
        {
            _PoolQueue[poolId].Enqueue(poolObject);
        }

        // Create poolable queue.
        public static void CreatePool(PoolableObject prefab, int poolSize)
        {
            prefab.gameObject.SetActive(false);
            var poolId = prefab.GetInstanceID();
            _PoolQueue[poolId] = new Queue<PoolableObject>(poolSize);
            for (int i = 0; i < poolSize; i++)
            {
                AddObjectToPool(prefab, poolId);
            }
        }

        // Instantiate next poolable object.
        public static PoolableObject GetNext(PoolableObject prefab, Vector3 pos, Quaternion rot, bool active = true)
        {
            var clone = GetNext(prefab);
            clone.transform.position = pos;
            clone.transform.rotation = rot;
            clone.gameObject.SetActive(active);
            return clone;
        }

        // Validate the instantiable object within the pool.
        public static PoolableObject GetNext(PoolableObject prefab)
        {
            var poolId = prefab.GetInstanceID();
            
            // If the current pool is not yet active within the scene.
            if (_PoolQueue.ContainsKey(poolId) == false || _PoolQueue[poolId].Count <= 0)
            {
                CreatePool(prefab, _INITIAL_POOL_SIZE);
            }

            var currentPoolableObjectList = _PoolQueue[poolId];

            for (int i = 0; i < currentPoolableObjectList.Count; i++)
            {
                // If the object is not active in the hierarchy, use that object.
                if (currentPoolableObjectList.Peek().gameObject.activeInHierarchy == false)
                {
                    return currentPoolableObjectList.Dequeue();
                }
            }

            return AddObjectToPool(prefab, poolId);
        }
    }
}

