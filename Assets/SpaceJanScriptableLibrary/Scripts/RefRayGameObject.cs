﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefRayGameObject")] 
public class RefRayGameObject : RefVariable<GameObject> {}

