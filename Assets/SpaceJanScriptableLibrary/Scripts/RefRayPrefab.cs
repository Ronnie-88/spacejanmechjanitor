﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefRayPrefab")]
public class RefRayPrefab : RefVariable<RayPrefab>
{
}
