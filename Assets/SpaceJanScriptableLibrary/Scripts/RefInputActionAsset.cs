﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefInputActionAsset")]
public class RefInputActionAsset : RefVariable<SpaceJanInputActionAsset>{}
