﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefVariable<T> : ScriptableObject
{
   private T _Value;
   public T Value
   {
      get { return _Value; }
      set { _Value = value; }
   }
   
   
}
