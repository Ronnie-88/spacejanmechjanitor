﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefQTEBehaviour")]
public class RefQTEBehaviour : RefVariable<QTEBehaviour>
{
}
