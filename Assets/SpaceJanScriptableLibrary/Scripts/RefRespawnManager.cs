﻿using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefRespawnManager")]
public class RefRespawnManager : RefVariable<RespawnManager>
{
}
