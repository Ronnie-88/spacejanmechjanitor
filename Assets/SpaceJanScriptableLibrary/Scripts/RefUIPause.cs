﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefUIPause")]
public class RefUIPause : RefVariable<UIPause>
{}
