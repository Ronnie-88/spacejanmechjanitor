﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefVector2")]
public class RefVector2 : RefVariable<Vector2> { }
