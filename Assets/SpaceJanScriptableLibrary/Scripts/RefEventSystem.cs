﻿using UnityEngine;
using UnityEngine.EventSystems;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefEventSystem")]
public class RefEventSystem : RefVariable<EventSystem>{}
