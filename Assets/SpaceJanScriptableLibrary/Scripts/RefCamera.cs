﻿using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefCamera")]
public class RefCamera : RefVariable<Camera> {}