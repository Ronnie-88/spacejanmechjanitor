﻿using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefUIManager")]
public class RefUserInterfaceManager : RefVariable<UIManager>
{
}
