﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefCameraManager")]
public class RefCameraManger : RefVariable<CameraManager> {}
