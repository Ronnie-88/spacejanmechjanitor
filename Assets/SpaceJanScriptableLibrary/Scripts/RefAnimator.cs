﻿using UnityEngine;

[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefAnimator")]
public class RefAnimator : RefVariable<Animator> {}
