﻿using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveLoadManager : MonoBehaviour
{
    public static SaveLoadManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        Load();
    }

    public SaveManager SavedData = new SaveManager();

    public void Save()
    {
        BinaryFormatter binary = new BinaryFormatter();
        FileStream fStream = File.Create(Application.persistentDataPath + "/save.data");

        SaveManager saver = new SaveManager();

        saver.CurrentGameData.MasterVolume = SavedData.CurrentGameData.MasterVolume;
        saver.CurrentGameData.DialogueVolume = SavedData.CurrentGameData.DialogueVolume;
        saver.CurrentGameData.MusicVolume = SavedData.CurrentGameData.MusicVolume;
        saver.CurrentGameData.SFXVolume = SavedData.CurrentGameData.SFXVolume;
        saver.CurrentGameData.GammaValue = SavedData.CurrentGameData.GammaValue;
        saver.CurrentGameData.CameraSensitivityX = SavedData.CurrentGameData.CameraSensitivityX;
        saver.CurrentGameData.CameraSensitivityY = SavedData.CurrentGameData.CameraSensitivityY;
        saver.CurrentGameData.InvertedY = SavedData.CurrentGameData.InvertedY;
        saver.CurrentGameData.CurrentCheckpointIndex = SavedData.CurrentGameData.CurrentCheckpointIndex;

        binary.Serialize(fStream, saver);
        fStream.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/save.data"))
        {
            BinaryFormatter binary = new BinaryFormatter();
            FileStream fStream = File.Open(Application.persistentDataPath + "/save.data", FileMode.Open);

            SaveManager saver = (SaveManager)binary.Deserialize(fStream);
            fStream.Close();

            SavedData.CurrentGameData.MasterVolume = saver.CurrentGameData.MasterVolume;
            SavedData.CurrentGameData.DialogueVolume = saver.CurrentGameData.DialogueVolume;
            SavedData.CurrentGameData.MusicVolume = saver.CurrentGameData.MusicVolume;
            SavedData.CurrentGameData.SFXVolume = saver.CurrentGameData.SFXVolume;
            SavedData.CurrentGameData.GammaValue = saver.CurrentGameData.GammaValue;
            SavedData.CurrentGameData.CameraSensitivityX = saver.CurrentGameData.CameraSensitivityX;
            SavedData.CurrentGameData.CameraSensitivityY = saver.CurrentGameData.CameraSensitivityY;
            SavedData.CurrentGameData.InvertedY = saver.CurrentGameData.InvertedY;
            SavedData.CurrentGameData.CurrentCheckpointIndex = saver.CurrentGameData.CurrentCheckpointIndex;
        }
        else
        {
            SavedData.CurrentGameData.MasterVolume = 1.0f;
            SavedData.CurrentGameData.DialogueVolume = 1.0f;
            SavedData.CurrentGameData.MusicVolume = 1.0f;
            SavedData.CurrentGameData.SFXVolume = 1.0f;
            SavedData.CurrentGameData.GammaValue = 0.5f;
            SavedData.CurrentGameData.CameraSensitivityX = 300f;
            SavedData.CurrentGameData.CameraSensitivityY = 2f;
            SavedData.CurrentGameData.InvertedY = true;
            SavedData.CurrentGameData.CurrentCheckpointIndex = 0;
        }
    }
}

[Serializable]
public class SaveManager
{
    public GameData CurrentGameData;
}

[Serializable]
public struct GameData
{
    public float MasterVolume;
    public float DialogueVolume;
    public float MusicVolume;
    public float SFXVolume;
    public float GammaValue;
    public float CameraSensitivityX;
    public float CameraSensitivityY;
    public bool InvertedY;
    public int CurrentCheckpointIndex;
}