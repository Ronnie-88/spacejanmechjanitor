﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField]
    private float _Speed = 5f;

    public List<Transform> waypoints = new List<Transform>();
    private Transform _TargetWaypoint;
    private int _TargetWaypointIndex = 0;
    private float _LastWaypointIndex;
    private float _MinDistance = 0.1f;
    private float _Distance;
    private float _CurrentSpeed;
    private bool _CanMove;

    [Header("RefVariables")]
    [SerializeField]
    protected RefQTEBehaviour _QTEBehaviour;
    [SerializeField]
    protected RefPushBack _RefPushBack;
    private AIBot _Bot;

    public event Action PauseWaypointMovement;

    private void Awake()
    {
        _Bot = GetComponent<AIBot>();
    }

    private void OnEnable()
    {
        _QTEBehaviour.Value.OnNotifyQTEStart += NotifyStopMove;
    }

    private void OnDisable()
    {
        _QTEBehaviour.Value.OnNotifyQTEStart -= NotifyStopMove;
    }

    private void Start()
    {
        //_TargetWaypoint = waypoints[_TargetWaypointIndex];
        _LastWaypointIndex = waypoints.Count;
    }

    private void Update()
    {
        Debug.Log($"{_TargetWaypointIndex}");
        DisableEnemyOnHardFail();
        // _Distance = Vector3.Distance(transform.position, _TargetWaypoint.position);
        CheckForPosition();
        _CurrentSpeed = _Speed * Time.deltaTime;
        MoveEnemy(_CurrentSpeed);
    }

    private void MoveEnemy(float speed)
    {
        if (_CanMove == false)
        {
            return;
        }
        if (_TargetWaypoint == null)
        {
            return;
        }

        transform.position = Vector3.MoveTowards(transform.position, _TargetWaypoint.position, speed);
    }

    // private void NewWaypoint()
    // {
    //     if (_Bot.GetBotType() == BotType.Hover)
    //     {
    //         Debug.Log("Move to new Waypoint");

    //         if (_Distance <= _MinDistance)
    //         {
    //             _TargetWaypointIndex++;
    //             UpdateTarget();
    //         }
    //     }
    // }

    public void NotifyCanMove()
    {
        _CanMove = true;
    }

    public void NotifyStopMove()
    {
        _CanMove = false;

    }

    private void CheckForPosition()
    {
        if (_TargetWaypoint == null)
        {
            return;
        }

        if (Vector3.Distance(transform.position, _TargetWaypoint.position) <= 0.5f)
        {
            NotifyStopMove();
            _TargetWaypoint = null;
        }
    }

    private void DisableEnemyOnHardFail()
    {
        if (_TargetWaypoint == waypoints[waypoints.Count - 1])
        {
            if (Vector3.Distance(transform.position, _TargetWaypoint.position) <= 0.5f)
            {
                this.transform.parent.gameObject.SetActive(false);
            }
        }
    }

    public void UpdateTarget()
    {
        _TargetWaypoint = waypoints[_TargetWaypointIndex];
        _TargetWaypointIndex++;
    }
}
