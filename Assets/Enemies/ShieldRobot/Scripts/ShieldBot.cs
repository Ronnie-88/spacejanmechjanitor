﻿using UnityEngine;

public class ShieldBot : AIBot
{
    // [Header("Wwise")]
    // [SerializeField]
    // private AK.Wwise.Event _PlayRunAudio;

    private void Start()
    {
        _Waypoints = GetComponent<Waypoints>();
    }

    private void OnTriggerEnter(Collider other)
    {
        // enable waypoint if player enters trigger
        _Waypoints.enabled = true;
    }
}
