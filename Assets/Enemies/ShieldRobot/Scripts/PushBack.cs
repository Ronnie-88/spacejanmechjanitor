﻿using UnityEngine;

public class PushBack : MonoBehaviour
{
    [Header("Player")]
    [SerializeField]
    private GameObject _Player;

    [Header("Push Back")]
    // [SerializeField]
    // private float _PushBackForce = 10f;
    // [SerializeField]
    // private float _Angle = 20f;
    [SerializeField]
    private float _ExplosionForce = 10.0f;
    [SerializeField]
    private Transform _ExplosionPositon;
    [SerializeField]
    private float _ExplosionRadius = 10.0f;
    [SerializeField]
    private float _UpwardsModifier = 0.0f;
    [SerializeField]
    private RefPushBack _RefPushBack;

    private Rigidbody _PlayerRigidbody;

    // [Header("Wwise")]
    // [SerializeField]
    // private AK.Wwise.Event _PushBackPlayerAudio;
    // [SerializeField]
    // private AK.Wwise.Event _PushBackFailAudio;


    private void Awake()
    {
        _RefPushBack.Value = this;
    }

    private void OnCollisionEnter(Collision other)
    {
        PlayerControllerManager player = other.gameObject.GetComponent<PlayerControllerManager>();

        if (player != null)
        {
            _PlayerRigidbody = player.GetComponent<Rigidbody>();
            
            // // play audio pushback success
            // _PushBackPlayerAudio.Post(this.gameObject);

            _PlayerRigidbody.AddExplosionForce(_ExplosionForce, _ExplosionPositon.position, _ExplosionRadius, _UpwardsModifier, ForceMode.VelocityChange);

            // Vector3 direction = Quaternion.AngleAxis(_Angle, Vector3.forward) * Vector3.up;
            // _PlayerRigidbody.AddForce(direction * _PushBackForce);

           // _PlayerRigidbody.AddForce((other.transform.position - transform.position) * _PushBackForce, ForceMode.Impulse);
            Debug.Log("Shield push");
        }
        else
        {
            // // play audio collision on other object
            // _PushBackFailAudio.Post(this.gameObject);
        }
    }
}
