﻿using System;
using UnityEngine;

public class HomingMissile : MonoBehaviour
{
    [Header("Player")]
    [SerializeField]
    private RefTransform _Target;
    private Rigidbody _PlayerRigidbody;

    [Header("Missile")]
    [SerializeField]
    private float _ShootForce = 10.0f;
    [SerializeField]
    private float _RotationForce = 10.0f;
    [SerializeField]
    private float _PushBackForce = 10.0f;
    [SerializeField]
    private float _HomingTime = 2.0f;

    [Header("Particles")]
    [SerializeField]
    private ParticleSystem _ExplosionParticles;

    [Header("QTEAssets")]
    [SerializeField]
    private RefQTEBehaviour _QTEBehaviourAsset;

    // [Header("Wwise")]
    // [SerializeField]
    // private AK.Wwise.Event _ExplodeAudio;

    private Rigidbody _Rigidbody;
    private Vector3 _Direction;
    private Vector3 _RotationAmount;
    private RawTimer _HomingTimer = new RawTimer();


    private void OnEnable()
    {
        _QTEBehaviourAsset.Value.OnNotifyQTEStart += KillProjectileInQTE;
        _HomingTimer.NotifyOnTick += TargetLock;
        _HomingTimer.NotifyOnTimerEnd += KeepMovingForward;
    }

    private void OnDisable()
    {
        _QTEBehaviourAsset.Value.OnNotifyQTEStart -= KillProjectileInQTE;
        _HomingTimer.NotifyOnTick -= TargetLock;
        _HomingTimer.NotifyOnTimerEnd -= KeepMovingForward;
    }

    private void Start()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        _HomingTimer.StartTimer(_HomingTime);
    }

    private void Update()
    {
        // _SelfDestructTimer += Time.deltaTime;
        // TargetLock();
        _HomingTimer.TimerTick(Time.deltaTime);
    }

    private void KeepMovingForward()
    {
        _Rigidbody.velocity = transform.forward * _ShootForce;
    }

    private void TargetLock()
    {
        // lock on player posiiton
        if (_Target.Value != null)
        {
            _Direction = (_Target.Value.position + new Vector3(0.0f, 3.0f, 0.0f)) - transform.position;
            _Direction.Normalize();
            _RotationAmount = Vector3.Cross(transform.forward, _Direction);
            _Rigidbody.angularVelocity = _RotationAmount * _RotationForce;
            _Rigidbody.velocity = transform.forward * _ShootForce;
        }
    }

    // when missile collides with an object
    private void OnCollisionEnter(Collision other)
    {
        // _ExplodeAudio.Post(this.gameObject);
        Instantiate(_ExplosionParticles, transform.position, Quaternion.identity);

        // get player
        PlayerControllerManager player = other.gameObject.GetComponent<PlayerControllerManager>();

        if (player != null)
        {
            _PlayerRigidbody = player.GetComponent<Rigidbody>();

            Vector3 direction = other.transform.position - transform.position;
            _PlayerRigidbody.AddForce(direction * _PushBackForce, ForceMode.Impulse);
        }

        Destroy(gameObject);
    }

    public void KillProjectileInQTE()
    {
        Instantiate(_ExplosionParticles, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

