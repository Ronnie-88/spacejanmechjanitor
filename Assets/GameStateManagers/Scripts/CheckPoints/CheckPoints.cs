﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoints : MonoBehaviour
{
  [SerializeField]
  private RefTransform _RespawnTransform;

  private void Awake()
  {
    _RespawnTransform.Value = transform;
  }
}
