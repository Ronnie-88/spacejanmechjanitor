﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseStates : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        DebugComponent player = other.gameObject.GetComponent<DebugComponent>();

        if (player != null)
        {
            // UIManager.Instance.ShowScreen<UILose>();
            player.ResetPosition();
        }
    }
}
