﻿using UnityEngine;

public class WinStates : MonoBehaviour
{
    [SerializeField]
    private RefUserInterfaceManager _UIManager;

    private void OnTriggerEnter(Collider other)
    {
        PlayerControllerManager player = other.gameObject.GetComponent<PlayerControllerManager>();

        if (player != null)
        {
            // You win
            if (_UIManager.Value == null)
            {
                Debug.Log("_UIManager.Value not in the scene.");
                return;
            }
            _UIManager.Value.ShowScreen<UIWin>();
        }
    }
}
