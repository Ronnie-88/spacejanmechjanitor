﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityController : MonoBehaviour
{
  [SerializeField]
  private float _VelocityThreshold = 0.0f;
  [SerializeField]
  private float _DefualtGravityScale = 1.0f;
  [SerializeField]
  private float _DescendingGravityScale = 1.0f;
  private float _GravityScale;
  private static float _GlobalGravity = -9.81f;
  private Rigidbody _RB;

  private void Awake()
  {
    _RB = GetComponent<Rigidbody>();
  }

  private void FixedUpdate()
  {
    ArtificialGravity();
  }

  public void ArtificialGravity()
  {
    if (_RB.velocity.y < _VelocityThreshold)
    {
      _GravityScale = _DescendingGravityScale;
    }
    else
    {
      _GravityScale = _DefualtGravityScale;
    }
    Vector3 gravity = _GlobalGravity * _GravityScale * Vector3.up;
    _RB.AddForce(gravity, ForceMode.Acceleration);
  }
}
