﻿using System;
using System.Collections;
using System.Collections.Generic;
using Console;
using UnityEngine;
using UnityEngine.InputSystem;

public class GroundController : PlayerController, IMovement, ILook, IJump
{
    /*This class is responsible for controlling the actions the player can perform when grounded
    /*Exposed Variables*/
    [Header("PlayerSettings")]
    [SerializeField, Range(0.0f, 1000.0f)]
    private float _PlayerMaxGroundLinearSpeedMagnitude;
    [SerializeField, Range(0.0f, 1000.0f)]
    private float _PlayerMaxGroundAngularSpeedMagnitude;
    [SerializeField]
    private RefRayGameObject _DetectedWallObject;

    [SerializeField]
    private AnimationCurve _PlayerAccelerationCurve;
    private float _PlayerAccelerationDuration;

    [SerializeField, Range(0, 10)]
    private int _NumberOfJumps;
    [SerializeField, Range(0, 10)]
    private int _NumberOfDashes;

    [Header("RefVariables")]
    [SerializeField]
    private RefFloat _PlayerJumpSpeedMagnitude;
    [SerializeField]
    private RefBool _IsSprintingBool;
    [SerializeField]
    private RefFloat _LastSetPlayerSpeed;
    [SerializeField]
    private RefBool _HasStoppedWallRideBool;
    /*Exposed Variables*/
    private float _CurrentTime = 0.0f;
    private float _PlayerAccelerationRatio;
    private float _PlayerAccelerationAlpha;
    private Keyframe _LastKeyframe;
    private RawTimer _IdleTimer;

    // [Header("Wwise")]
    // [SerializeField]
    // private AK.Wwise.Event _SingleJumpAudio;
    // [SerializeField]
    // private AK.Wwise.Event _LandingAudio;

    protected override void Awake()
    {
        base.Awake();
        _PlayerJumpCounter.Value = _NumberOfJumps;
        _PlayerDashCounter.Value = _NumberOfDashes;
        _LastKeyframe = _PlayerAccelerationCurve[_PlayerAccelerationCurve.length - 1];
        _PlayerAccelerationDuration = _LastKeyframe.time;

        _IdleTimer = new RawTimer();
    }

    private void Update()
    {
        
        AcceleratePlayer();
        _IdleTimer.TimerTick(Time.deltaTime);
        /////////////
        /// 
    }

    private void FixedUpdate()
    {
        PlayerRotationDetection();
        Move();
    }

    private void OnMovePerformed(InputAction.CallbackContext context)
    {
        _MovementInputDirection = context.ReadValue<Vector2>();
    }
    private void OnMoveCanceled(InputAction.CallbackContext context)
    {
        _MovementInputDirection = Vector2.zero;
        _PlayerAccelerationAlpha = 0.0f;

        _IdleTimer.StartTimer(5.0f);
    }

    private void OnLookPerformed(InputAction.CallbackContext context)
    {
        _CameraInputVector.Value = context.ReadValue<Vector2>();
    }
    private void OnLookCanceled(InputAction.CallbackContext context)
    {
        _CameraInputVector.Value = Vector2.zero;
    }

    private void OnJumpPerformed(InputAction.CallbackContext context)
    {
        if (_IsInAirBool.Value == false)
        {
            Jump();
        }
    }

    public void Move()
    {
        _MovementDirection = _DesiredLookDirection.Value;
        //_Rigidbody.velocity = (_MovementDirection * _CurrentPlayerSpeed * Time.fixedDeltaTime).WithY(_Rigidbody.velocity.y);

        if (_MovementInputDirection.magnitude != 0.0f)
        {
              _Rigidbody.AddForce((_MovementDirection.normalized * _PlayerMaxGroundLinearSpeedMagnitude * Time.fixedDeltaTime)
                        , ForceMode.VelocityChange);
        }
        else
        {
            _Rigidbody.velocity = new Vector3(0.0f, _Rigidbody.velocity.y, 0.0f);
        }
        _PlayerGroundSpeedMagnitude.Value = _Rigidbody.velocity.magnitude;
    }

    public void Look(float axisValueX, float axisValueY)
    {
        Vector3 camForward = _CameraTransform.Value.forward.normalized;
        Vector3 camRight = _CameraTransform.Value.right.normalized;

        camForward.y = 0.0f;
        camRight.y = 0.0f;

        _DesiredLookDirection.Value = (camForward * axisValueY) + (camRight * axisValueX);
 
        Vector3 cross = Vector3.Cross(transform.forward, _DesiredLookDirection.Value);
        float dot = Vector3.Dot(transform.forward, _DesiredLookDirection.Value);
        float angle = (1f - dot);

        angle *= Mathf.Sign(cross.y);
        _Rigidbody.AddTorque(transform.up * ( angle * _PlayerMaxGroundAngularSpeedMagnitude * Time.fixedDeltaTime)
            , ForceMode.VelocityChange);
    }

    public void Jump()
    {

        if (_PlayerJumpCounter.Value > 0)
        {
            _PlayerJumpCounter.Value--;
            _JumpVector = transform.up * _JumpMagnitude;
            _Rigidbody.AddForce(_JumpVector, ForceMode.Impulse);
            //_Rigidbody.velocity = _Rigidbody.velocity.WithY(_JumpMagnitude);
            // _SingleJumpAudio.Post(this.gameObject);
        }

    }

    private void PlayerRotationDetection()
    {
        Vector2 playerSpeed = new Vector2(_MovementInputDirection.x, _MovementInputDirection.y);

        if (playerSpeed.sqrMagnitude > 0.0f)
        {
            Look(_MovementInputDirection.x, _MovementInputDirection.y);
        }
        else
        {
            _DesiredLookDirection.Value = Vector3.zero;
        }
    }

    private void AccelerationTimer()
    {
        _CurrentTime += Time.deltaTime;
        if (_CurrentTime >= _PlayerAccelerationDuration)
        {
            _CurrentTime = _PlayerAccelerationDuration;
        }

        Mathf.Clamp(_CurrentTime, 0.0f, _PlayerAccelerationDuration);
        _PlayerAccelerationAlpha = _PlayerAccelerationCurve.Evaluate(_CurrentTime);
    }

    private void AcceleratePlayer()
    {

        if (_MovementInputDirection.normalized.sqrMagnitude >= 0.45f)
        {
            AccelerationTimer();
            _CurrentPlayerSpeed = Mathf.Lerp(0.0f, _PlayerMaxGroundLinearSpeedMagnitude, _PlayerAccelerationAlpha);
        }
        else
        {
            _CurrentTime = 0.0f;
            _CurrentPlayerSpeed = 0.0f;

        }
    }

    private void PlayerIdling()
    {
        _PedroAnimator.Value.SetTrigger("PlayerIsIdle");
        _IdleTimer.StartTimer(10f);
    }

    private void OnEnable()
    {
        _InputActionAsset.GroundInputMap.Enable();
        SetMyInputMap(_InputActionAsset.GroundInputMap);
        SubscribeInputActions();
        _PlayerJumpCounter.Value = _NumberOfJumps;
        _PlayerDashCounter.Value = _NumberOfDashes;
        _HasStoppedWallRideBool.Value = false;
        _GravityController.enabled = true;
        _DetectedWallObject.Value = null;
        // _LandingAudio.Post(this.gameObject);

        _IdleTimer.StartTimer(5.0f);
        _IdleTimer.NotifyOnTimerEnd += PlayerIdling;
    }

    private void OnDisable()
    {
        _InputActionAsset.GroundInputMap.Disable();
        _LastSetPlayerSpeed.Value = _CurrentPlayerSpeed;
        UnsubscribeInputActions();

        _IdleTimer.NotifyOnTimerEnd -= PlayerIdling;
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(1000, 50, 220, 20), $"Input Dir: {_MovementInputDirection}", GUI.skin.button);
    }

    protected override void SubscribeInputActions()
    {
        _InputActionAsset.GroundInputMap.Move.performed += OnMovePerformed;
        _InputActionAsset.GroundInputMap.Move.canceled += OnMoveCanceled;
        _InputActionAsset.GroundInputMap.Look.performed += OnLookPerformed;
        _InputActionAsset.GroundInputMap.Look.canceled += OnLookCanceled;
        _InputActionAsset.GroundInputMap.Jump.performed += OnJumpPerformed;
    }

    protected override void UnsubscribeInputActions()
    {
        _InputActionAsset.GroundInputMap.Move.canceled -= OnMoveCanceled;
        _InputActionAsset.GroundInputMap.Move.performed -= OnMovePerformed;
        _InputActionAsset.GroundInputMap.Look.performed -= OnLookPerformed;
        _InputActionAsset.GroundInputMap.Look.canceled -= OnLookCanceled;
        _InputActionAsset.GroundInputMap.Jump.performed -= OnJumpPerformed;
    }

    // Debug functions
    public void ChangePedroWalkSpeed()
    {
        //_PlayerWalkSpeedMagnitude = float.Parse(DebugConsole.Instance.InputParts[1]);
    }
    public void ChangePedroSprintSpeed()
    {
        _PlayerMaxGroundLinearSpeedMagnitude = float.Parse(DebugConsole.Instance.InputParts[1]);
    }
    public void ChangePedroJumpMagnitudeOnGround()
    {
        _JumpMagnitude = float.Parse(DebugConsole.Instance.InputParts[1]);
    }
}
