﻿using System;
using Cinemachine;
using UnityEngine;

public class GrindController : PlayerController
{
    // [Header("Wwise Events")]
    // [SerializeField]
    // private AK.Wwise.Event _RailGrindPlay = null;
    // [SerializeField]
    // private AK.Wwise.Event _RailGrindStop = null;
    private bool _IsFirstTime = true;

    private InteractWithTrack _TrackHook;   //holds functions that hook and unhook player from track
    private CinemachineSmoothPath _RailTrack;
    private CinemachineDollyCart _Cart;
    private CinemachineSmoothPath.Waypoint[] RailWaypoints;

    private void OnEnable()
    {
        SetMyInputMap(_InputActionAsset.GrindInputMap);
        if (_IsFirstTime == true)
        {
            _IsFirstTime = false;
        }
        else
        {
            // _RailGrindPlay.Post(this.gameObject);
        }

        _PedroAnimator.Value.SetTrigger("PlayerIsInGrindTrigger");
    }

    public void SetVariables(CinemachineSmoothPath railTrack, CinemachineDollyCart cart, InteractWithTrack trackHook)
    {
        _RailTrack = railTrack;
        _TrackHook = trackHook;
        _Cart = cart;

        RailWaypoints = _RailTrack.m_Waypoints;
    }

    private void Update()
    {
        StopGrind();
    }

    public void StopGrind()
    {
        if (RailWaypoints == null) return;
        if (_Cart.m_Position == RailWaypoints.Length - 1)
        {
            // _RailGrindStop.Post(this.gameObject);
            _TrackHook.GetOff(this.gameObject);
            RailWaypoints = null;
        }
    }
}
