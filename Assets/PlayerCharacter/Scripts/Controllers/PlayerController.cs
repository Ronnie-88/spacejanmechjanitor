﻿using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody))]
public abstract class PlayerController : MonoBehaviour
{
    //A player controller will contain actions that control a particular state of the player
    
    protected SpaceJanInputActionAsset _InputActionAsset;
    protected Rigidbody _Rigidbody;

    [SerializeField]
    private RefInputActionAsset _SpaceJanInputActionAsset;
    [SerializeField]
    protected RefTransform _CameraTransform;
    [SerializeField]
    protected RefVector3 _DesiredLookDirection;
    [SerializeField]
    protected RefVector2 _CameraInputVector;
    [SerializeField]
    protected RefFloat _PlayerGroundSpeedMagnitude;
    [SerializeField]
    protected RefBool _IsInAirBool;
    [SerializeField]
    protected RefInteger _PlayerJumpCounter;
    [SerializeField]
    protected RefInteger _PlayerDashCounter;
    [SerializeField]
    protected RefAnimator _PedroAnimator;
    
    [SerializeField, Range(0.0f, 3000.0f)]
    protected float _JumpMagnitude;

    protected Vector2 _MovementInputDirection;
    protected GravityController _GravityController;
    protected Vector3 _MovementDirection;
    protected float _CurrentPlayerSpeed;
    protected Vector3 _JumpVector;
    public InputActionMap MyInputMap { get; private set;}

    protected virtual void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        if (_SpaceJanInputActionAsset.Value == null) Debug.LogError("SpaceJanInputAsset is null");
        _Rigidbody = GetComponent<Rigidbody>();
        _CameraTransform.Value = Camera.main.transform;
        _InputActionAsset = _SpaceJanInputActionAsset.Value;
        _GravityController = GetComponent<GravityController>();
    }
    
    protected void SetMyInputMap( InputActionMap inputMap)
    {
        MyInputMap = inputMap;
    }
    protected virtual void SubscribeInputActions()
    {
        
    }
    protected virtual void UnsubscribeInputActions()
    {
        
    }
}
