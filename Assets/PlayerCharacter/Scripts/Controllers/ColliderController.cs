﻿using System;
using System.Collections;
using System.Collections.Generic;
using Console;
using UnityEngine;

public class ColliderController : MonoBehaviour
{
    //if the player is in air lerp the size of the colliders height and change center slightly up and reset
    [SerializeField]
    private float _NewPlayerCapsuleHeight;
    [SerializeField]
    private Vector3 _NewPlayerCapsuleCenter;
    [SerializeField]
    private RefBool _IsInAirBool;
    [SerializeField]
    private LayerMask _WallLayer;
    
    private CapsuleCollider _PlayerCapsule;
    private float _OriginalPlayerCapsuleHeight;
    private Vector3 _OriginalPlayerCapsuleCenter;
    private Rigidbody _Rigidbody;

    private void Awake()
    {
        _PlayerCapsule = GetComponent<CapsuleCollider>();
        _OriginalPlayerCapsuleHeight = _PlayerCapsule.height;
        _OriginalPlayerCapsuleCenter = _PlayerCapsule.center;
        _Rigidbody = GetComponent<Rigidbody>();
        //Debug.Log($"{_OriginalPlayerCapsuleCenter}, {_OriginalPlayerCapsuleHeight}");
    }

    private void Update()
    {
        AdjustCapsuleHeight();
        AdjustCapsuleCenter();
    }

    private void AdjustCapsuleHeight()
    {
        if (_IsInAirBool.Value == true)
        {
            _PlayerCapsule.height = Mathf.Lerp(_OriginalPlayerCapsuleHeight, _NewPlayerCapsuleHeight, 1.0f);
        }

        if (_IsInAirBool.Value == false)
        {
           _PlayerCapsule.height = Mathf.Lerp(_NewPlayerCapsuleHeight,_OriginalPlayerCapsuleHeight, 1.0f); 
        }
    }

    private void AdjustCapsuleCenter()
    {
        if (_Rigidbody.velocity.y <= 0)
        {
            _PlayerCapsule.center = Vector3.Lerp(_OriginalPlayerCapsuleCenter, _NewPlayerCapsuleCenter,  1.0f);
        }

        else
        {
            _PlayerCapsule.center = Vector3.Lerp(_NewPlayerCapsuleCenter, _OriginalPlayerCapsuleCenter, 1.0f);
        }
    }
}
