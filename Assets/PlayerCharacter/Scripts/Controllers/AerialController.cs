﻿using System;
using Console;
using UnityEngine;
using UnityEngine.InputSystem;

public class AerialController : PlayerController, IMovement, ILook, IJump, IDash
{

    [SerializeField]
    private RefBool _CanWallRideBool;

    [SerializeField, Range(0.0f, 1000.0f)]
    private float _PlayerMaxAerialLinearSpeedMagnitude;

    [SerializeField, Range(0.0f, 1000.0f)]
    private float _PlayerMaxAerialAngularSpeedMagnitude;

    [SerializeField]
    [Range(0.0f, 10.0f)]
    private float _DashDuration = 1.06f;

    [SerializeField]
    [Range(0.0f, 1700.0f)]
    private float _DashMagnitude = 55.2f;

    private bool _IsDashing;

    [SerializeField]
    private RefBool _IsWallRidingBool;

    [SerializeField]
    private RefFloat _LastSetPlayerSpeed;

    [SerializeField]
    private RefBool _CanDashBool;

    [SerializeField]
    private RefFloat _PlayerVerticalVelocity;

    private RawTimer _Timer;
    private float _AirControl = 0.0f;
    private float AlphaValue;
    private RefVector3 _SuccessfulWallRideNormal;
    private bool _IsWallRidePressed;
    private bool _IsFallSoundPlaying;


    // [Header("Wwise")]
    // [SerializeField]
    // private AK.Wwise.Event _DoubleJumpAudio;
    // [SerializeField]
    // private AK.Wwise.Event _DashAudio;
    // [SerializeField]
    // private AK.Wwise.Event _PlayFallAudio;
    // [SerializeField]
    // private AK.Wwise.Event _StopFallAudio;

    protected override void Awake()
    {
        base.Awake();
        _Timer = new RawTimer();
    }

    private void Update()
    {
        ValidateWallRideBool();
    }

    private void FixedUpdate()
    {
        PlayerRotationDetection();
        Move();
        _Timer.TimerTick(Time.fixedDeltaTime);
        PlayFallSound();
    }

    private void PlayerRotationDetection()
    {

        if (_IsDashing == true)
        {
            return;
        }
        var playerSpeed = new Vector2(_MovementInputDirection.x, _MovementInputDirection.y);

        if (playerSpeed.sqrMagnitude > 0.0f)
        {
            Look(_MovementInputDirection.x, _MovementInputDirection.y);
        }
        else
        {
            _DesiredLookDirection.Value = Vector3.zero;
        }

    }

    public void Dash()
    {
        if (_PlayerDashCounter.Value > 0)
        {
            StopFallSound();
            _IsFallSoundPlaying = false;
            if (_MovementDirection.normalized.magnitude == 0.0f)
            {
                _Rigidbody.AddForce(transform.forward * _DashMagnitude);
            }
            else
            {
                _Rigidbody.AddForce(_MovementDirection.normalized * _DashMagnitude);
            }
        }

    }

    public void Jump()
    {
        if (_PlayerJumpCounter.Value > 0)
        {
            _PedroAnimator.Value.SetTrigger("PlayerIsDoubleJumpingTrigger");
            StopFallSound();
            _IsFallSoundPlaying = false;
            _PlayerJumpCounter.Value--;
            _JumpVector = transform.up * _JumpMagnitude;
            _Rigidbody.AddForce(_JumpVector, ForceMode.Impulse);
            //_Rigidbody.velocity = _Rigidbody.velocity.WithY(_JumpMagnitude);
            // _DoubleJumpAudio.Post(this.gameObject);
        }
    }



    public void Move()
    {
        if (_IsDashing == true)
        {
            return;
        }
        _MovementDirection = _DesiredLookDirection.Value;

        _Rigidbody.AddForce((_MovementDirection.normalized * _PlayerMaxAerialLinearSpeedMagnitude * Time.fixedDeltaTime)
            , ForceMode.VelocityChange);
        _PlayerVerticalVelocity.Value = _Rigidbody.velocity.y;

    }
    public void Look(float axisValueX, float axisValueY)
    {
        var camForward = _CameraTransform.Value.forward.normalized;
        var camRight = _CameraTransform.Value.right.normalized;

        camForward.y = 0.0f;
        camRight.y = 0.0f;

        _DesiredLookDirection.Value = (camForward * axisValueY) + (camRight * axisValueX);

        Vector3 cross = Vector3.Cross(transform.forward, _DesiredLookDirection.Value);
        float dot = Vector3.Dot(transform.forward, _DesiredLookDirection.Value);
        float angle = (1f - dot);

        angle *= Mathf.Sign(cross.y);
        _Rigidbody.AddTorque(transform.up * (angle * _PlayerMaxAerialAngularSpeedMagnitude * Time.fixedDeltaTime)
            , ForceMode.VelocityChange);
    }


    private void OnEnable()
    {
        _InputActionAsset.AerialInputMap.Enable();
        SubscribeInputActions();
        SetMyInputMap(_InputActionAsset.AerialInputMap);
        /////
        _CanDashBool.Value = true;
        _Timer.NotifyOnTimerStart += PlayerIsDashing;
        _Timer.NotifyOnTick += Dash;
        _Timer.NotifyOnTimerEnd += OnNotifyTimerEnd;
        _CurrentPlayerSpeed = _LastSetPlayerSpeed.Value;
        _GravityController.enabled = true;
    }

    private void OnDisable()
    {
        _IsWallRidePressed = false;
        StopFallSound();
        _IsFallSoundPlaying = false;
        _Timer.OverrideTimer();
        _Timer.NotifyOnTimerStart -= PlayerIsDashing;
        _Timer.NotifyOnTick -= Dash;
        _Timer.NotifyOnTimerEnd -= OnNotifyTimerEnd;
        _InputActionAsset.AerialInputMap.Disable();
        UnsubscribeInputActions();
    }

    private void OnDashPerformed(InputAction.CallbackContext context)
    {
        if (_CanDashBool.Value == false) return;
        _Timer.StartTimer(_DashDuration);
    }

    private void OnDashCanceled(InputAction.CallbackContext context)
    {

    }

    private void OnMovePerformed(InputAction.CallbackContext context)
    {
        _MovementInputDirection = context.ReadValue<Vector2>();
    }

    private void OnMoveCanceled(InputAction.CallbackContext context)
    {
        _MovementInputDirection = Vector2.zero;
    }

    private void OnLookPerformed(InputAction.CallbackContext context)
    {
        _CameraInputVector.Value = context.ReadValue<Vector2>();
    }

    private void OnLookCanceled(InputAction.CallbackContext context)
    {
        _CameraInputVector.Value = Vector2.zero;
    }

    private void OnAerialJumpPerformed(InputAction.CallbackContext context)
    {
        Jump();
    }

    private void OnWallRideStarted(InputAction.CallbackContext context)
    {
        _IsWallRidePressed = context.ReadValue<float>() >= 0.45f;
    }

    private void OnWallRideCanceled(InputAction.CallbackContext context)
    {
        _IsWallRidePressed = false;
    }

    private void PlayerIsDashing()
    {
        _IsDashing = true;
        _GravityController.enabled = false;
        _CanDashBool.Value = false;
        // _DashAudio.Post(this.gameObject);
        _PedroAnimator.Value.SetTrigger("PlayerIsDashingTrigger");
    }

    private void PlayerIsNotDashing()
    {
        _IsDashing = false;
        _GravityController.enabled = true;
        _PlayerDashCounter.Value--;
        _PedroAnimator.Value.SetTrigger("PlayerStoppedDashingTrigger");
    }

    private void PlayFallSound()
    {
        if (_Rigidbody.velocity.y <= 0.0f && _IsFallSoundPlaying == false)
        {
            //Play sound here
            // _PlayFallAudio.Post(this.gameObject);
            _IsFallSoundPlaying = true;
        }
    }
    private void StopFallSound()
    {
        //Stop the sound
        // _StopFallAudio.Post(this.gameObject);
    }

    private void OnNotifyTimerEnd()
    {
        _Rigidbody.velocity = Vector3.zero;
        PlayerIsNotDashing();
    }

    private void ValidateWallRideBool()
    {
        if (_CanWallRideBool.Value == true)
        {
            if (_IsWallRidePressed == true)
            {
                _IsWallRidingBool.Value = true;
            }
            else
            {
                _IsWallRidingBool.Value = false;
            }
        }
        else
        {
            _IsWallRidingBool.Value = false;
        }
    }
    
    protected override void SubscribeInputActions()
    {
        _InputActionAsset.AerialInputMap.AerialMove.performed += OnMovePerformed;
        _InputActionAsset.AerialInputMap.AerialMove.canceled += OnMoveCanceled;
        _InputActionAsset.AerialInputMap.AerialLook.performed += OnLookPerformed;
        _InputActionAsset.AerialInputMap.AerialLook.canceled += OnLookCanceled;
        _InputActionAsset.AerialInputMap.AerialJump.performed += OnAerialJumpPerformed;
        _InputActionAsset.AerialInputMap.Dash.performed += OnDashPerformed;
        _InputActionAsset.AerialInputMap.Dash.canceled += OnDashCanceled;
        _InputActionAsset.AerialInputMap.WallRide.performed += OnWallRideStarted;
        _InputActionAsset.AerialInputMap.WallRide.canceled += OnWallRideCanceled;
    }

    protected override void UnsubscribeInputActions()
    {
        _InputActionAsset.AerialInputMap.WallRide.performed -= OnWallRideStarted;
        _InputActionAsset.AerialInputMap.WallRide.canceled -= OnWallRideCanceled;
        _InputActionAsset.AerialInputMap.AerialMove.performed -= OnMovePerformed;
        _InputActionAsset.AerialInputMap.AerialMove.canceled -= OnMoveCanceled;
        _InputActionAsset.AerialInputMap.AerialLook.performed -= OnLookPerformed;
        _InputActionAsset.AerialInputMap.AerialLook.canceled -= OnLookCanceled;
        _InputActionAsset.AerialInputMap.Dash.performed -= OnDashPerformed;
        _InputActionAsset.AerialInputMap.Dash.canceled -= OnDashCanceled;
        _InputActionAsset.AerialInputMap.AerialJump.performed -= OnAerialJumpPerformed;
    }

    // Debug functions

    public void ChangeDashDuration()
    {
        _DashDuration = float.Parse(DebugConsole.Instance.InputParts[1]);
    }

    public void ChangeDashForce()
    {
        _DashMagnitude = float.Parse(DebugConsole.Instance.InputParts[1]);
    }
}
