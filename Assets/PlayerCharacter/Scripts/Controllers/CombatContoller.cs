﻿using System;
using UnityEngine;

public class CombatContoller : PlayerController
{
    [SerializeField]
    private RefQTEBehaviour _QTEBehaviour;
    [SerializeField]
    private RefCameraManger _CameraManger;
    
    [SerializeField]
    private RefPlayerControllerManager _PlayerControllerManagerAsset;
    [SerializeField]
    private RefBool _IsInCombatBool;
    [SerializeField]
    private RefTransform _CombatPosition;
    [SerializeField,Range(0.0f, 10.0f)]
    private float _CombatTime;

    private bool _IsAttacking;
    private bool _CanAttack;
    private RawTimer _CombatTimer= new RawTimer(); 

    public event Action<InputType> OnActionButtonPressed;

    #region Unity Methods
    private new void Awake()
    {
        base.Awake();
        _InputActionAsset.CombatInputMap.Enable();
        SetMyInputMap(_InputActionAsset.CombatInputMap);
        _InputActionAsset.CombatInputMap.North.performed += context => UpdateEventAction(InputType.North);
        _InputActionAsset.CombatInputMap.South.performed += context => UpdateEventAction(InputType.South);
        _InputActionAsset.CombatInputMap.West.performed += context => UpdateEventAction(InputType.West);
        _InputActionAsset.CombatInputMap.East.performed += context => UpdateEventAction(InputType.East);
    }

    private void Update()
    { 
        _CombatTimer.TimerTick(Time.deltaTime);
    }

    private void FixedUpdate()
    {
        // if (_IsInCombat.Value == true && _IsAttacking)
        // {
        //     transform.LookAt(new Vector3(_CombatPosition.Value.position.x, transform.position.y, _CombatPosition.Value.position.z));
        //
        //     _Rigidbody.position = Vector3.Lerp(_Rigidbody.position, _CombatPosition.Value.position, Time.fixedDeltaTime);
        //
        //     if (_Rigidbody.position == _CombatPosition.Value.position)
        //     {
        //         _IsAttacking = false;
        //     }
        // }
    }

    private void OnEnable()
    {
        if (_QTEBehaviour.Value == null)
        {
            return;
        }
        
        _CombatTimer.NotifyOnTimerEnd += ExitCombatAnimSequence;
        OnActionButtonPressed += _QTEBehaviour.Value.EnterButtonInput;
        _QTEBehaviour.Value.OnSuccess += OnSuccessfulAttack;
        _QTEBehaviour.Value.OnFail += OnFailedAttack;
        StartTransitionIntoCombat();

        StartCombat();
    }

    private void OnDisable()
    {
        if (_QTEBehaviour.Value == null)
        {
            return;
        }
        _CombatTimer.NotifyOnTimerEnd -= ExitCombatAnimSequence;
        OnActionButtonPressed -= _QTEBehaviour.Value.EnterButtonInput;
        _QTEBehaviour.Value.OnSuccess -= OnSuccessfulAttack;
        _QTEBehaviour.Value.OnFail -= OnFailedAttack;
        
    }
    #endregion

    #region Internal Methods
    private void StartCombat()
    {
        _IsAttacking = true;

        _Rigidbody.velocity = Vector3.zero;
    }

    private void LaunchPlayer()
    {
        Debug.Log("PUSH FORWARD");
    }

    private void KnockbackPlayer()
    {
        Debug.Log("PUSH BACK");
        
    }
    #endregion

    #region QTE Methods
    private void OnSuccessfulAttack()
    {
        PlayOnSuccessCombatAnimSequence();
        _CombatTimer.StartTimer(_CombatTime);
        //LaunchPlayer();
    }

    private void OnFailedAttack()
    {
        PlayOnFailCombatAnimSequence();
        _CombatTimer.StartTimer(_CombatTime);
        KnockbackPlayer();
    }

    private void UpdateEventAction(InputType type)
    {
        if (OnActionButtonPressed == null) return;
        OnActionButtonPressed(type);
    }


    private void StartTransitionIntoCombat()
    {
        _PedroAnimator.Value.SetTrigger("PlayerIsInCombatTrigger");
    }
  

    private void PlayOnSuccessCombatAnimSequence()
    {
        _PedroAnimator.Value.SetTrigger("PlayerIsSuccessfulInCombatTrigger");
    }

    private void ExitCombatAnimSequence()
    {
        _CameraManger.Value.TransitionToPlayerCamera();
        _PedroAnimator.Value.SetTrigger("PlayerIsOutOfCombatTrigger");
        _IsInCombatBool.Value = false;
        _PlayerControllerManagerAsset.Value.CombatStateSwitch(_PlayerControllerManagerAsset.Value.LastPlayerState);
    }

    private void PlayOnFailCombatAnimSequence()
    {
        _PedroAnimator.Value.SetTrigger("PlayerIsFailedInCombatTrigger"); 
    }
    #endregion
}
