﻿using UnityEngine;

public class DebugComponent : MonoBehaviour
{
    [SerializeField]
    private RefUserInterfaceManager _UIManager;

    [SerializeField]
    private RefTransform _RespawnTransform;

    public void ResetPosition()
    {
        transform.position = _RespawnTransform.Value.position;
    }

    public void Win()
    {
        _UIManager.Value.ShowScreen<UIWin>();
    }
    
    public void Lose()
    {
        // _UIManager.Value.ShowScreen<UILose>();
    }
}
