﻿using UnityEngine;

public class PlayerCombatDetection : PlayerStateDetector
{
    [SerializeField]
    private RefQTEBehaviour _QTEBehaviourAsset;
    [SerializeField]
    private RefPlayerControllerManager _PlayerControllerManagerAsset;
    [SerializeField]
    private RefBool _IsInCombatBool;

    [SerializeField]
    private RefCameraManger _CameraManger;
    
    private void Start()
    {
        if(_QTEBehaviourAsset.Value == null) return;
        _QTEBehaviourAsset.Value.OnNotifyQTEStart += _CameraManger.Value.TransitionToQTECamera;
    }

    private void OnDisable()
    {
        if(_QTEBehaviourAsset.Value == null) return;
        _QTEBehaviourAsset.Value.OnNotifyQTEStart -= _CameraManger.Value.TransitionToQTECamera;
    }

    // public void ValueOnOnNotifyQTEEnd()
    // {
    //     _IsInCombatBool.Value = false;
    //     _PlayerControllerManagerAsset.Value.CombatStateSwitch( _PlayerControllerManagerAsset.Value.LastPlayerState);
    // }
}
