﻿using System.Collections.Generic;
using UnityEngine;

public class TestRaycastCache : MonoBehaviour
{
    [SerializeField]
    private float _MaxDistance;

    private readonly List<TransformHit> _OrderedRays = new List<TransformHit>();

    [SerializeField]
    private Transform[] _TestRayOrigins;


    private void Update()
    {
        _TestRayOrigins.PopulateTransformHits(_OrderedRays, _MaxDistance);
        DrawDebugRays();
    }


    private void DrawDebugRays()
    {
#if UNITY_EDITOR
        var trh = _OrderedRays[0];
        TryDarTransformHitRay(trh, Color.blue);

        for (var i = 1; i < _OrderedRays.Count; i++)
        {
            trh = _OrderedRays[i];
            TryDarTransformHitRay(trh, Color.green);
        }
#endif
    }

    private void TryDarTransformHitRay(TransformHit trh, Color color)
    {
        if (trh.HasHit)
            Debug.DrawRay(trh.OriginTransform.position, trh.OriginTransform.forward * trh.Hit.distance, color);
        else
            Debug.DrawRay(trh.OriginTransform.position, trh.OriginTransform.forward * _MaxDistance, Color.red);
    }
}

public class TransformHit
{
    public bool HasHit;
    public RaycastHit Hit;
    public Transform OriginTransform;
}
