﻿using UnityEngine;

public abstract class PlayerStateDetector : MonoBehaviour
{
    [SerializeField, Range(0.0f, 10.0f)]
    protected float _DetectionOffset;
    [SerializeField]
    protected RefBool _IsInAirBool;
    [SerializeField]
    protected RefBool _IsWallRunning;

    protected Collider _PlayerCapsule;

    [SerializeField]
    protected LayerMask _DetectorLayerMask;

    protected virtual void Awake()
    {
        _PlayerCapsule = GetComponent<Collider>();
    }

    protected virtual void Update() { }
}
