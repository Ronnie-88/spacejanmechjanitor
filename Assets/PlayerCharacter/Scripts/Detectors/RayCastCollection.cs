﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastCollection : MonoBehaviour
{
    [SerializeField]
    private List<RayPrefab> _WallRaysThatHit;

    private RayPrefab _ShortestHitRay = null;

    [SerializeField]
    private RefRayPrefab _ShortestRayPrefab;
    
    [SerializeField]
    private RefBool _IsWallRidingBool;
    
    [SerializeField]
    private RefBool _IsWallRidingRightBool;

    [SerializeField]
    private RefBool _HasStoppedWallRideBool;

    [SerializeField, Range(0.0f, 5.0f)]
    private float _TimeBeforeCanStick;

    private Vector3 _NormalOffset;
    private RawTimer _WallTimer;
    [SerializeField]
    private RefBool _CanStartWallTimerBool;

    private bool _ShouldResetShortestRayPrefab;
    

    private void Update()
    {
        _NormalOffset = new Vector3(0.0f, 3.0f, 0.0f);
        ClosestRay();
        DrawAllRays(Color.green, Color.blue);
    }

    public void DrawAllRays(Color hitColour, Color closestRayColour)
    {
        for (int i = 0; i < _WallRaysThatHit.Count; i++)
        {
            if (_WallRaysThatHit[i].RayHit() && _WallRaysThatHit[i].Ray.Equals(_ShortestHitRay.Ray))
            {
                Debug.DrawRay(_WallRaysThatHit[i].Ray.origin,
                    _WallRaysThatHit[i].Ray.direction * _WallRaysThatHit[i].MaxDistance, closestRayColour);
                Debug.DrawRay(_WallRaysThatHit[i].RayCastHit.point + _NormalOffset,
                    _WallRaysThatHit[i].RayCastHit.normal * 3.0f, Color.cyan);
            }
            else if (_WallRaysThatHit[i].RayHit())
            {
                Debug.DrawRay(_WallRaysThatHit[i].Ray.origin,
                    _WallRaysThatHit[i].Ray.direction * _WallRaysThatHit[i].MaxDistance, hitColour);
            }
            else
            {
                Debug.DrawRay(_WallRaysThatHit[i].Ray.origin,
                    _WallRaysThatHit[i].Ray.direction * _WallRaysThatHit[i].MaxDistance, Color.red);
            }
        }
    }

    public void ClosestRay()
    {
        for (int i = 0; i < _WallRaysThatHit.Count; i++)
        {
            if (_WallRaysThatHit[i].RayHit())
            {
                _ShortestHitRay = _WallRaysThatHit[i];
            }
        }

        //if more rays hit, the second one is compared against all other rays to get shortest one
        for (int i = 0; i < _WallRaysThatHit.Count; i++)
        {
            if (_WallRaysThatHit[i].RayHit() && _ShortestHitRay.RayHit())
            {
                if (_WallRaysThatHit[i].RayCastHit.distance < _ShortestHitRay.RayCastHit.distance)
                {
                    _ShortestHitRay = _WallRaysThatHit[i];
                }
            }
        }

        _ShortestRayPrefab.Value = _ShortestHitRay;
    }
    
/*
-first we need to know when the player entered a wall ride.~
-we need to know when the player has stopped wallriding.~

-if hasstoppedwallriding is true then turn on timer
-method go through the list and turn off rays based on bools.
- when timer is ticks to zero we re-enable rays.
*if grounded then re-enable rays as well* 
*/
}
