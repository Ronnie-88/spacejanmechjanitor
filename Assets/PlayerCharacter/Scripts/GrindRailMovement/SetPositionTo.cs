﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPositionTo : MonoBehaviour
{
    [SerializeField] private Vector3 _NewPos;
    [SerializeField] private bool _Local = true;

    public void SetPosition()
    {
        if (_Local)
        {
            transform.localPosition = _NewPos;
            transform.localRotation = Quaternion.identity;
        }
        else
        {
            transform.position = _NewPos;
        }
    }
    
    
}
