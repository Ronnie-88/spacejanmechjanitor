﻿using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(CinemachineDollyCart))]
public class UpdateCartToPositionInTrack : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _VCam;
    private CinemachineDollyCart _Cart;

    private void OnEnable()
    {
        if(_VCam == null) return;
        _Cart = GetComponent<CinemachineDollyCart>();

        var body = _VCam.GetCinemachineComponent<CinemachineTrackedDolly>();
        _Cart.m_Position = body.m_PathPosition;
    }
}
