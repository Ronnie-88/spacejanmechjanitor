﻿using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public static List<Checkpoint> CheckpointList = new List<Checkpoint>();

    [SerializeField]
    private RefRespawnManager _RespawnManager;
    [SerializeField]
    private int _CheckpointIndex;
    public int CurrentCheckpointIndex
    {
        get { return _CheckpointIndex; }
        private set { }
    }

    private void Awake()
    {
        if (_RespawnManager.Value == null)
        {
            Debug.LogError($"The RespawnManager does not exit in the Scene!");
            return;
        }
    }

    private void OnEnable()
    {
        if (CheckpointList.Contains(this) == false)
        {
            CheckpointList.Add(this);
        }
    }

    private void OnDisable()
    {
        CheckpointList.Clear();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        PlayerControllerManager player = other.GetComponent<PlayerControllerManager>();

        if (player != null)
        {
            if (_CheckpointIndex == 0)
            {
                _RespawnManager.Value.SetNewCheckpoint(transform.parent);
                gameObject.SetActive(false);
            }

            if (_CheckpointIndex <= _RespawnManager.Value.CurrentCheckpointIndex)
            {
                return;
            }
            _RespawnManager.Value.CurrentCheckpointIndex = _CheckpointIndex;
            _RespawnManager.Value.SetNewCheckpoint(transform.parent);
        }
    }
}
