﻿using UnityEngine;

public class KillZVolume : MonoBehaviour
{
    [SerializeField]
    private RefRespawnManager _RespawnManager;

    private void OnTriggerEnter(Collider other)
    {
        PlayerControllerManager player = other.GetComponent<PlayerControllerManager>();

        if (player != null)
        {
            _RespawnManager.Value.Respawn();
        }
    }
}
