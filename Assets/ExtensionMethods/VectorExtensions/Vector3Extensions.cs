﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions 
{
    public static Vector3 WithY(this Vector3 v3, float y)
    {
        return new Vector3(v3.x, y, v3.z);
    }
}
