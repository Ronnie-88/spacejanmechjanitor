﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PreloadSettings : MonoBehaviour
{
    [Header("Post-Processing References")]
    [SerializeField]
    private PostProcessProfile _PPP;

    [Header("Camera Sensitivity References")]
    [SerializeField]
    private RefBool _InvertedCamera;
    [SerializeField]
    private RefFloat _CameraSensitivityX;
    [SerializeField]
    private RefFloat _CameraSensitivityY;

    private ColorGrading _ColorGradingModel;

    #region Initializing Methods
    private void Awake()
    {
        GetSettings();

        LoadSettings();

        LockCursor();
    }

    private void GetSettings()
    {
        _PPP.TryGetSettings(out _ColorGradingModel);
    }

    private void LockCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void LoadSettings()
    {
        LoadVolumeSettings();
        LoadGammaSettings();
        LoadCameraSettings();
    }

    private void LoadVolumeSettings()
    {
        // AkSoundEngine.SetRTPCValue("MasterVolume", SaveLoadManager.Instance.SavedData.CurrentGameData.MasterVolume);
        // AkSoundEngine.SetRTPCValue("DialogueVolume", SaveLoadManager.Instance.SavedData.CurrentGameData.DialogueVolume);
        // AkSoundEngine.SetRTPCValue("MusicVolume", SaveLoadManager.Instance.SavedData.CurrentGameData.MusicVolume);
        // AkSoundEngine.SetRTPCValue("SFXVolume", SaveLoadManager.Instance.SavedData.CurrentGameData.SFXVolume);
    }

    private void LoadGammaSettings()
    {
        float gammaValue = Mathf.Lerp(-1f, 1f, SaveLoadManager.Instance.SavedData.CurrentGameData.GammaValue);
        _ColorGradingModel.postExposure.value = gammaValue;
    }

    private void LoadCameraSettings()
    {
        _CameraSensitivityX.Value = SaveLoadManager.Instance.SavedData.CurrentGameData.CameraSensitivityX;
        _CameraSensitivityY.Value = SaveLoadManager.Instance.SavedData.CurrentGameData.CameraSensitivityY;
        _InvertedCamera.Value = SaveLoadManager.Instance.SavedData.CurrentGameData.InvertedY;
    }
    #endregion
}
