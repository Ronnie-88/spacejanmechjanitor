﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class UISlider : MonoBehaviour
{
    [SerializeField]
    private RefUserInterfaceManager _UIManager;

    private Slider _Slider;

    public UnityEvent OnSliderValueChanged;

    private void Awake()
    {
        _Slider = GetComponent<Slider>();

        _Slider.onValueChanged.AddListener(delegate { OnValidatedSliderChanged(); });
    }

    private void OnValidatedSliderChanged()
    {
        if (_UIManager.Value.CurrentObjectSelected() == this.gameObject)
        {
            OnSliderValueChanged?.Invoke();
        }
    }
}
