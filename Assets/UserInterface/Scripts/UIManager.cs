﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [Header("SO References"), SerializeField]
    private RefUserInterfaceManager _UIManager;
    [SerializeField]
    private RefInputActionAsset _SpaceJanInputAsset;

    [Header("Canvas Fields")]
    [SerializeField]
    private RefCamera _MainCamera;
    [SerializeField]
    private UICanvas _UICanvas;

    [Header("Main Menu Activation Fields")]
    [SerializeField]
    private int _SceneBuildIndex = 2;
    [SerializeField]
    private float _TimeToLoadMainMenu = 1.5f;

    private bool _IsPlaying;
    private bool _FirstTimeLoading;

    private EventSystem _EventSystem;

    private SpaceJanInputActionAsset _Controls;

    private InputSystemUIInputModule _InputModule;

    private UIScreen _CurrentUIScreen;

    private Dictionary<Type, UIScreen> _Screens;
    private UILoadingElements[] _LoadingElements;

    private UIFadeScreen _FadeScreen;

    private Button _TransitionButton;

    private IEnumerator _CurrentCoroutine;

    public event Action OnStartPerformed;
    public event Action OnSubmitPerformed;
    public event Action OnSubmitCancelled;
    public event Action OnCancelPerformed;

    public event Action OnPause;
    public event Action OnUnpause;

    #region Unity Methods
    private void Awake()
    {
        GetComponents();
        GetScreens();
        SetButtonSubscribers();
        SetFirstTimePlaying();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == _SceneBuildIndex && _FirstTimeLoading)
        {
            _FirstTimeLoading = false;
            _CurrentCoroutine = StartGameAfterSeconds(_TimeToLoadMainMenu);
            StartCoroutine(_CurrentCoroutine);
        }

        SetMainCamera();
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    #endregion

    #region Initializing Methods
    private void GetScreens()
    {
        // Fade to black screen.
        _FadeScreen = GetComponentInChildren<UIFadeScreen>(true);
        _FadeScreen.gameObject.SetActive(true);

        // Rest of the User Interface screens. 
        _Screens = new Dictionary<Type, UIScreen>();

        foreach (var screen in GetComponentsInChildren<UIScreen>(true))
        {
            screen.gameObject.SetActive(false);
            _Screens[screen.GetType()] = screen;
        }
    }

    private void GetComponents()
    {
        _UIManager.Value = this;
        _EventSystem = GetComponent<EventSystem>();
        _InputModule = GetComponent<InputSystemUIInputModule>();
        _LoadingElements = GetComponentsInChildren<UILoadingElements>(true);
        _Controls = _SpaceJanInputAsset.Value;

        if (_UICanvas == null) _UICanvas = GetComponentInChildren<UICanvas>();
    }

    private void SetMainCamera()
    {
        _UICanvas.CurrentCanvas().worldCamera = _MainCamera.Value;

        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case 2:
                SetCameraDepth(7.0f);
                break;
            case 3:
                SetCameraDepth(4.1f);
                break;
        }
    }

    private void SetButtonSubscribers()
    {
        _Controls.UserInterfaceInputMap.Enable();

        _Controls.UserInterfaceInputMap.Submit.performed += DefaultSubmitPerformed;
        _Controls.UserInterfaceInputMap.Submit.canceled += DefaultSubmitCanceled;
        _Controls.UserInterfaceInputMap.Cancel.performed += DefaultCancelPerformed;
        _Controls.UserInterfaceInputMap.Pause.performed += DefaultStartPerformed;
    }

    private void SetFirstTimePlaying()
    {
        _FirstTimeLoading = true;
    }

    #endregion

    #region Screen Methods
    public void ShowScreen<T>() where T : UIScreen
    {
        if (_CurrentUIScreen != null)
        {
            if (_CurrentUIScreen.IsAnimated() == false)
            {
                _CurrentUIScreen.gameObject.SetActive(false);
            }
            else if (_CurrentUIScreen.IsAnimated() == true)
            {
                _CurrentUIScreen.ExitScreen();
            }
        }

        var varType = typeof(T);

        if (_Screens.ContainsKey(varType) == false) return;

        _CurrentUIScreen = _Screens[varType];

        // If the screen to show is still active, disable it. 
        if (_CurrentUIScreen.gameObject.activeInHierarchy == true)
        {
            _CurrentUIScreen.gameObject.SetActive(false);
        }

        _CurrentUIScreen.gameObject.SetActive(true);

        _EventSystem.SetSelectedGameObject(null);

        // Select the first button from an interactable screen. 
        if (_CurrentUIScreen.IsInteractable() == true)
        {
            if (_TransitionButton == null)
            {
                _EventSystem.SetSelectedGameObject(_CurrentUIScreen.FirstSelectedButton());
            }
            else
            {
                _EventSystem.SetSelectedGameObject(_TransitionButton.gameObject);
                _TransitionButton = null;
            }
        }
    }

    public void FadeIntoScene<T>(int sceneToLoad, bool respawn = false, float time = 1.0f) where T : UIScreen
    {
        _FadeScreen.StartFadeIntoScene<T>(sceneToLoad, respawn, time);
    }

    public void FadeIn(float time = 1.0f)
    {
        _FadeScreen.StartFade(true, time);
    }

    public void FadeOut(float time = 1.0f)
    {
        _FadeScreen.StartFade(false, time);
    }
    #endregion

    #region Public Methods & Variables
    public void SetSelectedButton(Button button)
    {
        _EventSystem.SetSelectedGameObject(button.gameObject);
    }

    public void SetTransitionButton(Button button)
    {
        _TransitionButton = button;
    }

    public void SetPlayState(bool playState)
    {
        _IsPlaying = playState;

        if (playState == true)
        {
            Time.timeScale = 1.0f;
        }

        else if (playState == false)
        {
            Time.timeScale = 0.0f;
        }
    }

    public void SetLoadingItemsActive(bool active)
    {
        foreach (var item in _LoadingElements)
        {
            item.gameObject.SetActive(active);
        }
    }

    public void SetCameraDepth(float depth)
    {
        _UICanvas.CurrentCanvas().planeDistance = depth;
    }

    public bool IsPlaying()
    {
        return _IsPlaying;
    }

    public UIScreen CurrentScreen()
    {
        return _CurrentUIScreen;
    }

    public GameObject CurrentObjectSelected()
    {
        return _EventSystem.currentSelectedGameObject;
    }

    public EventSystem GetEventSystem()
    {
        if (_EventSystem == null) Debug.Log("Event System is missing.");
        return _EventSystem;
    }

    public InputSystemUIInputModule GetInputModule()
    {
        if (_InputModule == null) Debug.Log("Input module is missing.");
        return _InputModule;
    }

    public void StartGame()
    {
        FadeIn();
        SetPlayState(true);
        ShowScreen<UIStartScreen>();
    }

    public void InvokePause()
    {  
        OnPause?.Invoke();
    }

    public void InvokeUnpause()
    {
        OnUnpause?.Invoke();
    }
    #endregion

    #region Action Callers
    private void DefaultStartPerformed(InputAction.CallbackContext context)
    {
        OnStartPerformed?.Invoke();
    }

    private void DefaultSubmitPerformed(InputAction.CallbackContext context)
    {
        OnSubmitPerformed?.Invoke();
    }

    private void DefaultSubmitCanceled(InputAction.CallbackContext context)
    {
        OnSubmitCancelled?.Invoke();
    }

    private void DefaultCancelPerformed(InputAction.CallbackContext context)
    {
        OnCancelPerformed?.Invoke();
    }
    #endregion 

    #region Scene Methods
    public void LoadNewScene(int id)
    {
        SceneManager.LoadScene(id);
    }

    private IEnumerator StartGameAfterSeconds(float time)
    {
        yield return new WaitForSeconds(time);
        StartGame();

        StopCoroutine(_CurrentCoroutine);
    }
    #endregion
}
