﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIFadeScreen : MonoBehaviour
{
    [Header("Scriptable Object References")]
    [SerializeField]
    private RefUserInterfaceManager _UIManager;
    [SerializeField]
    private RefBool _CanPause;

    [Header("Waiting Times")]
    [SerializeField]
    private float _SceneChangeTime = 3.0f;
    [SerializeField]
    private float _RespawnTime = 1.0f;

    private CanvasGroup _CanvasGroup;
    private IEnumerator _CurrentCoroutine;
    private IEnumerator _SubCoroutine;

    #region Unity Methods
    private void Awake()
    {
        GetComponents();
    }
    #endregion

    #region Initializing Methods
    private void GetComponents()
    {
        _CanvasGroup = GetComponent<CanvasGroup>();
    }
    #endregion

    #region Coroutine Methods
    private IEnumerator FadeScreen(bool fadingIn, float transitionTime = 1.0f)
    {
        float time = 0f;

        float targetValue;

        if (fadingIn == true)
        {
            targetValue = 0.0f;
            _CanvasGroup.alpha = 1.0f;
        }
        else
        {
            targetValue = 1.0f;
            _CanvasGroup.alpha = 0.0f;
        }

        while (time < transitionTime)
        {
            time += Time.unscaledDeltaTime;

            _CanvasGroup.alpha = Mathf.Lerp(_CanvasGroup.alpha, targetValue, Mathf.SmoothStep(0.0f, 1.0f, time / transitionTime));

            if (time >= transitionTime)
            {
                _CanvasGroup.alpha = targetValue;

                if (_SubCoroutine != null)
                {
                    StopCoroutine(_SubCoroutine);
                }
            }

            yield return null;
        }
    }

    private IEnumerator FadeIntoScene<T>(int sceneToLoad, bool respawn, float transitionTime) where T : UIScreen
    {
        _UIManager.Value.GetInputModule().enabled = false;

        _UIManager.Value.SetLoadingItemsActive(true);

        _CanPause.Value = false;

        yield return StartCoroutine(FadeScreen(false, transitionTime / 2.0f));

        var asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad);

        while (asyncLoad.isDone == true)
        {
            yield return null;
        }

        _UIManager.Value.ShowScreen<T>();

        yield return new WaitForSeconds(respawn ? _RespawnTime : _SceneChangeTime);


        yield return StartCoroutine(FadeScreen(true, transitionTime / 2.0f));

        _UIManager.Value.SetLoadingItemsActive(false);

        _CanPause.Value = true;

        _UIManager.Value.GetInputModule().enabled = true;

        InvalidateCorutines();
    }

    private void ValidateCurrentCoroutine()
    {
        if (_CurrentCoroutine != null)
        {
            StopCoroutine(_CurrentCoroutine);
        }
    }

    private void ValidateSubCoroutine()
    {
        if (_SubCoroutine != null)
        {
            StopCoroutine(_SubCoroutine);
        }
    }

    private void InvalidateCorutines()
    {
        _CurrentCoroutine = null;
        _SubCoroutine = null;
        StopAllCoroutines();
    }
    #endregion

    #region Fade Screen Public Methods
    public void StartFadeIntoScene<T>(int sceneToLoad, bool respawn = false, float time = 1.0f) where T : UIScreen
    {
        ValidateCurrentCoroutine();

        _CurrentCoroutine = FadeIntoScene<T>(sceneToLoad, respawn, time);
        StartCoroutine(_CurrentCoroutine);
    }

    public void StartFade(bool fadingIn, float transitionTime = 1.0f)
    {
        ValidateSubCoroutine();

        _SubCoroutine = FadeScreen(fadingIn, transitionTime);
        StartCoroutine(_SubCoroutine);
    }
    #endregion
}
