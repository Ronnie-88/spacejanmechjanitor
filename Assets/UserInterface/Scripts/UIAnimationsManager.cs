﻿using UnityEngine;

public class UIAnimationsManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private RefUserInterfaceAnimationsManager _UIAnimationManager;

    #region Unity Methods
    private void Awake()
    {
        GetComponents();
    }
    #endregion

    #region Initializing Methods
    private void GetComponents()
    {
        _UIAnimationManager.Value = this;
    }
    #endregion
}
