﻿using System;
using UnityEngine;

public class UIMainMenuConfirmation : UIScreen
{
    [SerializeField]
    private RefUIPause _UIPause;

    public void Confirm()
    {
        _UIManager.Value.SetPlayState(true);
        _UIManager.Value.SetTransitionButton(null);
        _UIManager.Value.FadeIntoScene<UIMainMenu>(2);
        _UIManager.Value.InvokeUnpause();
    }

    public void Reject()
    {
        _UIManager.Value.ShowScreen<UIPause>();
    }

    protected override void CustomOnCancelPerformed()
    {
        Reject();
    }
}
