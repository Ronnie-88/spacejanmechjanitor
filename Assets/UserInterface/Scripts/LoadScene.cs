﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadScene : MonoBehaviour
{
    [SerializeField]
    private string _SceneName;

    [SerializeField]
    private bool _LoadOnEnable;

    private void OnEnable()
    {
        if (_LoadOnEnable)
        {
            LoadSceneNow();
        }
    }

    public void LoadSceneNow()
    {
        SceneManager.LoadScene(_SceneName);
    }
}
