﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIAudioSliders : MonoBehaviour
{
    [SerializeField]
    private Slider _MasterVolume;
    [SerializeField]
    private Slider _DialogueVolume;
    [SerializeField]
    private Slider _MusicVolume;
    [SerializeField]
    private Slider _SFXVolume;

    [SerializeField]
    private TextMeshProUGUI _MasterText;
    [SerializeField]
    private TextMeshProUGUI _DialogueText;
    [SerializeField]
    private TextMeshProUGUI _MusicText;
    [SerializeField]
    private TextMeshProUGUI _SFXText;

    // #region Wwise
    // private void SetMasterVolume() => AkSoundEngine.SetRTPCValue("MasterVolume", _MasterVolume.value);
    // private void SetDialogueVolume() => AkSoundEngine.SetRTPCValue("DialogueVolume", _DialogueVolume.value);
    // private void SetMusicVolume() => AkSoundEngine.SetRTPCValue("MusicVolume", _MusicVolume.value);
    // private void SetSFXVolume() => AkSoundEngine.SetRTPCValue("SFXVolume", _SFXVolume.value);
    // #endregion

    #region Unity Methods
    private void Awake()
    {
        SubscribeSetters();
    }

    private void OnEnable()
    {
        LoadSettings();
    }
    #endregion

    #region Slider Methods
    private void SubscribeSetters()
    {
        // _MasterVolume.onValueChanged.AddListener(delegate { SetMasterVolume(); });
        // _DialogueVolume.onValueChanged.AddListener(delegate { SetDialogueVolume(); });
        // _MusicVolume.onValueChanged.AddListener(delegate { SetMusicVolume(); });
        // _SFXVolume.onValueChanged.AddListener(delegate { SetSFXVolume(); });
    }

    private void LoadSettings()
    {
        _MasterVolume.value = SaveLoadManager.Instance.SavedData.CurrentGameData.MasterVolume;
        _DialogueVolume.value = SaveLoadManager.Instance.SavedData.CurrentGameData.DialogueVolume;
        _MusicVolume.value = SaveLoadManager.Instance.SavedData.CurrentGameData.MusicVolume;
        _SFXVolume.value = SaveLoadManager.Instance.SavedData.CurrentGameData.SFXVolume;
        DisplayNumbers();
    }

    public void ApplyVolumeChanges()
    {
        SaveLoadManager.Instance.SavedData.CurrentGameData.MasterVolume = _MasterVolume.value;
        SaveLoadManager.Instance.SavedData.CurrentGameData.DialogueVolume = _DialogueVolume.value;
        SaveLoadManager.Instance.SavedData.CurrentGameData.MusicVolume = _MusicVolume.value;
        SaveLoadManager.Instance.SavedData.CurrentGameData.SFXVolume = _SFXVolume.value;
        SaveLoadManager.Instance.Save();
    }

    public void RevertVolumeChanges()
    {
        _MasterVolume.value = 1f;
        _DialogueVolume.value = 1f;
        _MusicVolume.value = 1f;
        _SFXVolume.value = 1f;
    }

    public void DisplayNumbers()
    {
        _MasterText.text = Mathf.RoundToInt(_MasterVolume.value * 100).ToString();
        _DialogueText.text = Mathf.RoundToInt(_DialogueVolume.value * 100).ToString();
        _MusicText.text = Mathf.RoundToInt(_MusicVolume.value * 100).ToString();
        _SFXText.text = Mathf.RoundToInt(_SFXVolume.value * 100).ToString();
    }
    #endregion
}
