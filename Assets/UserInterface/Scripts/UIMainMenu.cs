﻿using UnityEngine;

public class UIMainMenu : UIScreen
{
    private new void OnEnable()
    {
        base.OnEnable();
        Time.timeScale = 1.0f;
    }

    public void Play()
    {
        SaveLoadManager.Instance.SavedData.CurrentGameData.CurrentCheckpointIndex = 0;
        SaveLoadManager.Instance.Save();
        
        _UIManager.Value.SetPlayState(true);
        _UIManager.Value.FadeIntoScene<UIInGame>(3);
    }

    public void Credits()
    {
        _UIManager.Value.ShowScreen<UICredits>();
    }

    public void Options()
    {
        _UIManager.Value.ShowScreen<UIOptions>();
    }

    public void Exit()
    {
        Application.Quit();
    }
}
