﻿using UnityEngine;

public class UIWin : UIScreen
{
    private new void OnEnable()
    {
        base.OnEnable();
        _UIManager.Value.SetPlayState(false);
    }

    public void MainMenu()
    {
        _UIManager.Value.ShowScreen<UIMainMenu>();
        _UIManager.Value.LoadNewScene(2);
    }
}
