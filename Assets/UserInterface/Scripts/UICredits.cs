﻿using UnityEngine;

public class UICredits : UIScreen
{
    private void Back()
    {
        _UIManager.Value.ShowScreen<UIMainMenu>();
    }

    protected override void CustomOnSubmitPerformed()
    {
        _Animator.speed = 10;
    }

    protected override void CustomOnSubmitCancelled()
    {
        _Animator.speed = 1;
    }

    protected override void CustomOnCancelPerformed()
    {
        _Animator.speed = 1;
        Back();
    }
}
