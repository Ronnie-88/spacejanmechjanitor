﻿using UnityEngine;

public class UIInGame : UIScreen
{
    [Header("Boolean References")]
    [SerializeField]
    private RefBool _IsInCombat;
    [SerializeField]
    private RefBool _CanPause;

    protected override void CustomOnStartPerformed()
    {
        if(_CanPause.Value == false || _IsInCombat.Value == true) return;

        _UIManager.Value.SetPlayState(false);
        _UIManager.Value.ShowScreen<UIPause>();
        _UIManager.Value.InvokePause();
    }
}
