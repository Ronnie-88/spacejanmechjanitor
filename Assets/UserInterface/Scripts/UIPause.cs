﻿public class UIPause : UIScreen
{
    public void Resume()
    {
        _UIManager.Value.SetPlayState(true);
        _UIManager.Value.ShowScreen<UIInGame>();
        _UIManager.Value.InvokeUnpause();
    }

    public void Options()
    {
        _UIManager.Value.ShowScreen<UIOptions>();
    }

    public void Controls()
    {
        _UIManager.Value.ShowScreen<UIControls>();
    }

    public void MainMenu()
    {
        _UIManager.Value.ShowScreen<UIMainMenuConfirmation>();
    }

    public void Quit()
    {
        _UIManager.Value.ShowScreen<UIExitConfirmation>();
    }
    
    protected override void CustomOnCancelPerformed()
    {
        Resume();
    }

    protected override void CustomOnStartPerformed()
    {
        Resume();
    }
}
