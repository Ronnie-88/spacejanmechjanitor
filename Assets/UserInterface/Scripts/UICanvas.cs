﻿using UnityEngine;

public class UICanvas : MonoBehaviour
{
    private Canvas _Canvas;

    #region Initializing Methods
    private void Awake()
    {
        GetComponents();
    }

    private void GetComponents()
    {
        _Canvas = GetComponent<Canvas>();
    }
    #endregion

    #region Canvas Methods
    public void SwitchCanvas(bool inGameSpace = false)
    {
        if (inGameSpace == true) _Canvas.renderMode = RenderMode.ScreenSpaceCamera;
        else if (inGameSpace == false) _Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
    }
    #endregion

    #region Public Methods
    public Canvas CurrentCanvas()
    {
        return _Canvas;
    }
    #endregion
}
