﻿using UnityEngine;
using UnityEngine.Events;

public class UIOptions : UIScreen
{
    [Space(20)]
    public UnityEvent OnBack;

    public void Back()
    {
        if (_UIManager.Value.IsPlaying() == false)
        {
            _UIManager.Value.ShowScreen<UIPause>();
        }
        else
        {
            _UIManager.Value.ShowScreen<UIMainMenu>();
        }
    }

    protected override void CustomOnCancelPerformed()
    {
        Back();
        OnBack?.Invoke();
    }
}