﻿using UnityEngine;

public class UIIcon : MonoBehaviour
{
    private Animator _Animator;

    private void Awake()
    {
        _Animator = GetComponent<Animator>();
    }

    public void NormalIcon()
    {
        _Animator.SetTrigger("Normal");
    }

    public void SelectIcon()
    {
        _Animator.SetTrigger("Selected");
    }

    public void SubmitIcon()
    {
        _Animator.SetTrigger("Pressed");
    }
}
