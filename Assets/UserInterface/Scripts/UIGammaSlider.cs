﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;
using TMPro;

public class UIGammaSlider : MonoBehaviour
{
    [SerializeField]
    private PostProcessProfile _Profile;

    [SerializeField]
    private Slider _GammaSlider;

    [SerializeField]
    private TextMeshProUGUI _Text;

    private ColorGrading _ColorGradingModel;

    #region Unity Methods
    private void Awake()
    {
        _Profile.TryGetSettings(out _ColorGradingModel);

        LoadSettings();
        SubscribeSetters();
    }

    private void OnEnable()
    {
        LoadSettings();
    }
    #endregion

    #region Slider Methods
    private void SetGammaValue()
    {
        _ColorGradingModel.postExposure.value = GammaValue();
    }

    private void SubscribeSetters()
    {
        _GammaSlider.onValueChanged.AddListener(delegate { SetGammaValue(); });
    }

    private void LoadSettings()
    {
        _GammaSlider.value = SaveLoadManager.Instance.SavedData.CurrentGameData.GammaValue;
    }

    public void ApplyGammaChanges()
    {
        SaveLoadManager.Instance.SavedData.CurrentGameData.GammaValue = _GammaSlider.value;
        SaveLoadManager.Instance.Save();
    }

    public void RevertGammaChanges()
    {
        _GammaSlider.value = 0.5f;
    }

    private float GammaValue()
    {
        float gammaValue = Mathf.Lerp(-1.0f, 1.0f, _GammaSlider.value);
        return gammaValue;
    }

    public void DisplayNumbers()
    {
        _Text.text = GammaValue().ToString("0.0");
    }
    #endregion
}
