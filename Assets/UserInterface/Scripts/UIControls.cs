﻿public class UIControls : UIScreen
{
    private void Back()
    {
        _UIManager.Value.ShowScreen<UIPause>();
    }

    protected override void CustomOnCancelPerformed()
    {
        Back();
    }
}
