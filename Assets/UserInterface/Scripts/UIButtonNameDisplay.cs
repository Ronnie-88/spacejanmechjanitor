﻿using UnityEngine;
using TMPro;

public class UIButtonNameDisplay : MonoBehaviour
{
    private TextMeshProUGUI _Text;

    private void Awake()
    {
        GetComponents();
    }

    private void GetComponents()
    {
        _Text = GetComponent<TextMeshProUGUI>();
    }

    public void SetText(string buttonName)
    {
        _Text.text = buttonName.ToUpper();
    }
}
