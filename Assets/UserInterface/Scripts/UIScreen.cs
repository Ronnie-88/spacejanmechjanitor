﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using GameSavvy.OpenUnityAttributes;

[RequireComponent(typeof(CanvasGroup))]
public class UIScreen : MonoBehaviour
{
    [Header("References"), SerializeField]
    protected RefUserInterfaceManager _UIManager;

    [Header("Interactions")]
    [SerializeField]
    private bool _IsAnimated;
    [Space(5)]
    [SerializeField]
    private bool _IsInteractable;
    [SerializeField, ShowIf("_IsInteractable")]
    protected Selectable _FirstElementSelected;

    [Space(10)]
    [SerializeField, ShowIf("_IsInteractable")]
    protected bool _CustomButtonOnStart;
    [SerializeField, ShowIf("_CustomButtonOnStart")]
    protected Button _ButtonToSelectOnStart;

    [Space(10)]
    [SerializeField, ShowIf("_IsInteractable")]
    protected bool _CustomButtonOnBack;
    [SerializeField, ShowIf("_CustomButtonOnBack")]
    protected Button _ButtonToSelectOnBack;

    [Space(10)]
    [SerializeField, ShowIf("_IsInteractable")]
    private bool _HasExitScreenEvent;
    [SerializeField, ShowIf("_HasExitScreenEvent")]
    protected UnityEvent _ExitScreen;

    private RectTransform _RectTransform;
    protected Button _LastButtonPressed;
    protected Animator _Animator;

    private UIDisabler[] _ElementsToDisable;
    private bool _HasElementsToDisable;

    #region Unity Methods
    protected void Awake()
    {
        ValidateInputs();
        GetComponents();
        ResetScreenTransform();
    }

    protected void OnEnable()
    {
        SubscribeCustomActions();

        if (_IsAnimated)
        {
            AwakeScreen();
        }
    }

    protected void OnDisable()
    {
        UnsubscribeCustomActions();

        DisableUIElements();
    }
    #endregion

    #region Initializing Methods
    private void ValidateInputs()
    {
        // Check that the first element to select on the screen is assigned
        if (_IsInteractable == true && _FirstElementSelected == null)
        {
            Debug.LogError("First Element Selected Button is not assigned.");
            return;
        }
    }

    private void GetComponents()
    {
        _RectTransform = GetComponent<RectTransform>();
        _ElementsToDisable = GetComponentsInChildren<UIDisabler>(true);

        if (_IsAnimated)
        {
            _Animator = GetComponent<Animator>();
        }

        if (_ElementsToDisable.Length > 0)
        {
            _HasElementsToDisable = true;
        }
        else
        {
            _HasElementsToDisable = false;
        }
    }

    private void SubscribeCustomActions()
    {
        _UIManager.Value.OnStartPerformed += OnStartPressed;
        _UIManager.Value.OnSubmitPerformed += OnSubmitHold;
        _UIManager.Value.OnSubmitCancelled += OnSubmitRelease;
        _UIManager.Value.OnCancelPerformed += OnCancelPressed;
    }

    private void UnsubscribeCustomActions()
    {
        _UIManager.Value.OnStartPerformed -= OnStartPressed;
        _UIManager.Value.OnSubmitPerformed -= OnSubmitHold;
        _UIManager.Value.OnSubmitCancelled -= OnSubmitRelease;
        _UIManager.Value.OnCancelPerformed -= OnCancelPressed;
    }
    #endregion

    #region UI Element Methods
    private void DisableUIElements()
    {
        if (_HasElementsToDisable == false) return;

        foreach (var element in _ElementsToDisable)
        {
            element.gameObject.SetActive(false);
        }
    }
    #endregion

    #region Public Methods
    public GameObject FirstSelectedButton()
    {
        return _FirstElementSelected.gameObject;
    }

    public bool IsAnimated()
    {
        return _IsAnimated;
    }

    public bool IsInteractable()
    {
        return _IsInteractable;
    }

    public void SetLastButtonPressed(Button button = null)
    {
        _UIManager.Value.SetTransitionButton(button);
    }

    public void AwakeScreen()
    {
        _Animator.SetTrigger("Awake");
    }

    public void ExitScreen()
    {
        _Animator.SetTrigger("Exit");

        OnExitScreen();
    }
    #endregion

    #region Protected Methods
    protected void ResetScreenTransform()
    {
        _RectTransform.anchoredPosition = Vector3.zero;
        _RectTransform.localScale = Vector3.one;
    }

    protected void DisableScreen()
    {
        gameObject.SetActive(false);
    }

    protected void OnStartPressed()
    {
        if (_UIManager.Value.CurrentScreen() != this) return;

        if (_CustomButtonOnStart == true)
        {
            _UIManager.Value.SetSelectedButton(_ButtonToSelectOnStart);
        }

        CustomOnStartPerformed();
    }

    protected void OnSubmitHold()
    {
        if (_UIManager.Value.CurrentScreen() != this) return;

        CustomOnSubmitPerformed();
    }

    protected void OnSubmitRelease()
    {
        if (_UIManager.Value.CurrentScreen() != this) return;

        CustomOnSubmitCancelled();
    }

    protected void OnCancelPressed()
    {
        if (_UIManager.Value.CurrentScreen() != this) return;

        if (_CustomButtonOnBack == true)
        {
            _UIManager.Value.SetSelectedButton(_ButtonToSelectOnBack);
        }

        CustomOnCancelPerformed();
    }

    protected virtual void OnExitScreen()
    {
        _ExitScreen?.Invoke();
    }

    protected virtual void CustomOnStartPerformed() { }
    protected virtual void CustomOnSubmitPerformed() { }
    protected virtual void CustomOnSubmitCancelled() { }
    protected virtual void CustomOnCancelPerformed() { }
    #endregion
}
