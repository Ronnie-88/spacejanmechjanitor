﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillObject : MonoBehaviour
{
    [SerializeField]
    private float _LifeTime;
    
    private void Awake()
    {
        Destroy(gameObject, _LifeTime);    
    }
    
}
