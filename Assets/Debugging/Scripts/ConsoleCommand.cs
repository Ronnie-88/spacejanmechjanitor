using UnityEngine;

namespace Console   // Use 'namespace Console' on all/new ConsoleCommand related scripts
{
    // Parent abstract class used to create all other commands
    public abstract class ConsoleCommand
    {
        public abstract string Command { get; protected set; }          // Name used to identify the command
        public abstract string Description { get; protected set; }      // Description of the command
        public abstract string Help { get; protected set; }             // Tip on how to use the command

        private float FloatToValidate;

        // Adds the command to the DebuggingConsole's dictionary
        public void AddCommandToConsole()
        {
            DebugConsole.AddCommandsToConsole(Command, this);
        }

        // Attempts to run command
        public void TryRunCommand()
        {
            if (!ValidateInput()) return;
            RunCommand();
        }

        // Validates the input
        public virtual bool ValidateInput()
        {
            return ValidateSingleCommand();
        }

        // Validates single named commands
        public bool ValidateSingleCommand()
        {
            // Checks if only the command name was written
            if (DebugConsole.Instance.InputParts.Length >= 2)
            {
                Debug.LogWarning("Additional input detected. Please enter the command name only.");
                return false;
            }
            else return true;
        }

        // Validates a command that takes another command to run the command
        public bool ValidateCommandCommand()
        {
            // If the input doesn't contain another string
            if (DebugConsole.Instance.InputParts.Length <= 1)
            {
                Debug.LogWarning($"Missing command name. Try {Command} <b>command name</b>' instead.");
                return false;
            }
            // iIf the command has extra inputs
            else if (DebugConsole.Instance.InputParts.Length >= 3)
            {
                Debug.LogWarning($"The {Command} command can only take one argument.");
                return false;
            }

            // If the command is not recognized in the dictionary
            if (!DebugConsole.Commands.ContainsKey(DebugConsole.Instance.InputParts[1]))
            {
                Debug.LogWarning($"'{DebugConsole.Instance.InputParts[1]}' command not recognized");
                return false;
            }
            // If the command is within the dictionary
            else if (DebugConsole.Commands.ContainsKey(DebugConsole.Instance.InputParts[1]))
            {
                return true;
            }

            // Return false for unknown error
            else return false;
        }

        // Validates a command that takes a float to run the command
        public bool ValidateFloatCommand()
        {
            // If the input doesn't contain another string
            if (DebugConsole.Instance.InputParts.Length <= 1)
            {
                Debug.LogWarning($"Missing number. Try '{Command} <b>number</b>' instead.");
                return false;
            }
            // If the command has extra inputs
            else if (DebugConsole.Instance.InputParts.Length >= 3)
            {
                Debug.LogWarning("Additional input detected. Please enter the command name followed by a number only.");
                return false;
            }

            // If the input is not a float
            if (!float.TryParse(DebugConsole.Instance.InputParts[1], out FloatToValidate))
            {
                Debug.LogWarning("No numbers detected. Please enter a number");
                return false;
            }
            // If the input is a float
            else if (float.TryParse(DebugConsole.Instance.InputParts[1], out FloatToValidate))
            {
                return true;
            }

            // Return false for unknown error
            else return false;
        }

        // Runs the command's custom method
        public abstract void RunCommand();
    }
}
